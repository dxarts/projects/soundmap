Analyses {
	var pathName;
	var <files, <data, <normTypes, <offset, analysis;

	*new { |pathName|
		^super.newCopyArgs(pathName).init
	}

	init {
		files = case
		{ pathName.isKindOf(String) } { PathName(pathName).deepFiles }
		{ pathName.isKindOf(PathName) } { pathName.deepFiles }
		// assume array of paths
		{ pathName }
	}

	convertToSignals {
		// convert files to signals
		^files.collect{ |pathname|
			Signal.read(pathname.fullPath);
		};
	}

	mixMultiChannels { |destFolder|
		files.do{ |pathname|
			var writeSF, readSF, rawData;
			readSF = SoundFile.openRead(pathname.fullPath);
			writeSF = SoundFile.new.headerFormat_(readSF.headerFormat).sampleFormat_(readSF.sampleFormat).sampleRate_(readSF.sampleRate);
			destFolder = destFolder ?? { pathname.pathOnly.replace("raw", "samples") };
			writeSF.openWrite(destFolder ++ pathname.fileName);
			rawData = FloatArray.newClear(readSF.numFrames * readSF.numChannels);
			readSF.readData(rawData);
			rawData = FloatArray.newFrom(rawData.clump(readSF.numChannels).collect{ |frame| frame[0] });
			writeSF.writeData(rawData);
			writeSF.close

		}

	}

	fft { |fftSize = 512, hopSize = 0.25, sampleRate = 48000, maxHops, minHops, average|
		var minSize, numHops, signals;

		signals = this.convertToSignals;

		// make hopSize in samples
		hopSize = (fftSize * hopSize).asInteger;

		// find smallest signal
		minSize = signals.collect(_.size).minItem;

		// calculate smallest number of hops to be used for each file
		numHops = (minSize/hopSize).asInteger;

		// note: could result in larger hop size
		maxHops.notNil.if({
			(numHops > maxHops).if({ numHops = maxHops })
		});

		// note: could result in smaller hop size, since numHops is calculated from smallest sound file
		minHops.notNil.if({
			(numHops < minHops).if({ numHops = minHops })
		});

		data = signals.collect{ |signal|
			Analysis.new(signal, sampleRate).fft(fftSize, hopSize, numHops, average).sum/numHops
		};

	}

	singleAnalysis { |file, analysisType, args, fftSize, hopSize, clumpSize, fileNum, dataNum, thisCond|
		var average;
		{
			case
			{ analysisType == 'duration' } {
				data[fileNum][dataNum + offset] = analysis.duration(file)
			}
			{ analysisType == 'fft' } {
				var tempData;
				args.notNil.if({
					offset = args[0] - 1
				});
				tempData = analysis.fft(file, fftSize, hopSize, averages: args.notNil.if({ args[0] }) );
				tempData.do{ |binData, inc|
					data[fileNum][dataNum + inc] = binData
				}

			}
			{
				analysis.perform(analysisType, file, fftSize: fftSize, hopSize: hopSize, args: args, action: { |tempData|
					// reduce data (for now)
					(tempData.isKindOf(Array) or: tempData.isKindOf(FloatArray)).if({
						var dat;
						dat = tempData.clump(clumpSize).collect{ |chunk| chunk.sum/clumpSize };
						(tempData.size.mod(clumpSize) > 0).if({
							dat = dat.drop(-1)
						});

						data[fileNum][dataNum + offset] = dat
					}, {
						data[fileNum][dataNum + offset] = tempData
					});
					thisCond.test_(true).signal
				});

			}
		}.forkIfNeeded

	}

	analysisData { |analysisTypes, fftSize = 2048, hopSize = 0.5, sampleRate = 48000, loadCondition, verbose = false|
		{
			var averagedSize, clumpSize, cond, tmpPath;
			tmpPath = Platform.defaultTempDir +/+ "DataToNumpy.txt";
			analysis = Analysis.new(sampleRate);
			clumpSize = (fftSize/Server.default.options.blockSize/2).asInteger;
			offset = 0;
			this.parseDataTypes(analysisTypes);
			data = files.collect{ nil };
			cond = Condition.new;

			files.do{ |file, fileNum|

				data[fileNum] = normTypes.collect{ nil };

				analysisTypes.do{ |analysisTypeArgs, dataNum|
					var analysisType, args;
					#analysisType, args = analysisTypeArgs;
					this.singleAnalysis(file, analysisType, args, fftSize, hopSize, clumpSize, fileNum, dataNum, cond);
					cond.wait;
					cond.test_(false);


				};

			};
			verbose.if({ "Analysis Finished!".postln });
			loadCondition !? { loadCondition.test_(true).signal }
		}.forkIfNeeded

	}

	nearestNeighbor {
		^NearestNeighbor(this.data)
	}

	nearestIndex { |data, analysisTypes|
		var dataAxes;
		analysisTypes.notNil.if({
			dataAxes = analysisTypes.collect{ |dataType|
				normTypes.indicesOfEqual(dataType)
			}.flat;
		});

		^this.nearestNeighbor.nearestIndex(data, dataAxes)

	}

	parseDataTypes { |dataTypes|

		normTypes = [];
		dataTypes = dataTypes.collect{ |dataTypeArgs|
			var dataType, args;

			dataTypeArgs.isKindOf(Array).if({
				#dataType, args = dataTypeArgs
			}, {
				dataType = dataTypeArgs
			});

			case
			{ dataType == 'mfcc' }
			{
				args.isNil.if({ args = [13]});
				args[0].do{
					normTypes = normTypes.add('mfcc')
				}
			}
			{ dataType == 'chromagram' }
			{
				args.isNil.if({ args = [12] });
				args[0].do{
					normTypes = normTypes.add('chromagram')
				}
			}
			{ dataType == 'spectralEntropy' }
			{
				args.isNil.if({ args = [9] });
				args[0].do{
					normTypes = normTypes.add('spectralEntropy')
				}
			}
			{ dataType == 'tartini' }
			{
				normTypes = normTypes.add('tartiniFreq');
				normTypes = normTypes.add('tartiniHasFreq')
			}
			{ dataType == 'beatTrack' }
			{
				3.do{
					normTypes = normTypes.add('beatTrack')
				};
				normTypes = normTypes.add('beatTrackT')

			}
			{ dataType == 'fft' }
			{
				args.isNil.if({ args = [8] });
				args[0].do{
					normTypes = normTypes.add('fft')
				}
			}
			{
				normTypes = normTypes.add(dataType)
			}
		}
	}

}