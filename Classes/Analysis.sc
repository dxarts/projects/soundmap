// wrapper for analysis, mainly for SoundMap
// dependencies: wslib, mir

Analysis {
	var <>sampleRate, <>sound;
	var <tmpPath, <soundFilePath;
	var <numHops, <writeBuffer;
	var nonfftTypes, numChannels;
	var <multiData, <server;

	*new{ |sampleRate|
		^super.newCopyArgs(sampleRate).init
	}

	init {
		nonfftTypes = [Tartini, Pitch];
		sampleRate = sampleRate ?? { 48000 }
	}

	checkForSignal { |sound|
		^sound.isKindOf(Signal).if({
			sound
		}, {
			sound.isKindOf(PathName).if({
				Signal.read(sound.fullPath)
			}, {
				Signal.read(sound)
			})

		})
	}

	checkForFilePath { |sound|
		^sound.isKindOf(Signal).if({
			tmpPath = Platform.defaultTempDir +/+ "Analysis" ++ UniqueID.next ++ ".wav";
			sound.write(tmpPath, sampleRate: sampleRate);
			tmpPath
		}, {
			sound.isKindOf(PathName).if({
				sound.fullPath
			}, {
				sound
			})
		})
	}

	// fft on entire sound
	fft { |signal, fftSize = 512, hopSize = 0.25, numHops, average, args|
		var thisHop, mags, index, data;

		signal = this.checkForSignal(signal);

		// unpack args if necessary
		args.notNil.if({
			#hopSize, numHops, average = args
		});

		// if necessary, recalculate hop size based on number of hops
		// for the smallest file, should be the same as hopSize, for larger files, hop will be larger
		numHops.notNil.if({
			thisHop = (signal.size/numHops).asInteger;
		}, {
			thisHop = (fftSize * hopSize).asInteger;
			numHops = (signal.size/thisHop).round.asInteger;
		});

		index = 0;
		^data = numHops.collect{ |hop|
			var windowedSig, fft, mag;

			// take window of signal
			windowedSig = ((index + (fftSize - 1)) > (signal.size - 1)).if({
				signal[index..].zeroPad(fftSize)
			}, {
				signal[index..index + (fftSize - 1)]
			});
			windowedSig = windowedSig * Signal.hanningWindow(fftSize);

			// take fft
			fft = windowedSig.rfft(Signal.rfftCosTable(fftSize/2 + 1));
			mag = fft.magnitude.keep((fftSize/2).asInteger);

			// average if necessary
			average.notNil.if({
				(average > 0).if({
					mag = (mag.size/average).asInteger.collect{ |thisMagNum|
						mag[(thisMagNum * average)..(((thisMagNum + 1) * average) - 1)].squared.sum.sqrt/average
					}
				})
			});

			// skip by new hop size
			index = index + thisHop;

			mag
		}.flop;

	}

	mir { |sound, analysisType, fftSize, hopSize, numChannels = 1, args|
		var signalBuffer, dur, synth, sf, data;
		var score, tmp, bufferSize, tmpFile, synthName;

		server = Server.default;

		server.newAllocators;

		soundFilePath = this.checkForFilePath(sound);

		sf = SoundFile.openRead(soundFilePath);

		tmp = Platform.defaultTempDir +/+ "MIR" ++ UniqueID.next ++ ".wav";

		tmpFile = PathName.tmp +/+ UniqueID.next ++ ".osc";

		dur = sf.duration;

		sf.close;

		signalBuffer = Buffer.new(server);

		bufferSize = dur * sampleRate / server.options.blockSize;

		writeBuffer = Buffer.new(server, bufferSize, numChannels);

		synthName = ('AnalysisSynth'  ++ UniqueID.next).asSymbol;

		synth = SynthDef(synthName, { |buffer|
			var fft, sig, mir;

			sig = PlayBuf.ar(1, buffer, BufRateScale.kr(buffer));

			mir = nonfftTypes.includes(analysisType).if({
				analysisType.kr(sig, *args)[0]
			}, {
				fft = FFT(LocalBuf(fftSize), sig, hopSize, wintype:1);

				analysisType.kr(fft, *args)
			});

			RecordBuf.kr(mir, writeBuffer)

		});

		score = Score([
			[0, writeBuffer.allocMsg],
			[0, signalBuffer.allocReadChannelMsg(soundFilePath, channels: 0)],
			[0, [\d_recv, synth.asBytes]],
			[0, Synth.basicNew(synthName, server).newMsg(0, ['buffer', signalBuffer.bufnum])],
			[dur, writeBuffer.writeMsg(tmp, headerFormat: "WAV", sampleFormat: "float")]
		]);

		score.saveToFile("~/Desktop/test.sc".standardizePath);

		score.recordNRTSync(tmpFile, "/dev/null", sampleRate: sampleRate,
			options: ServerOptions.new.verbosity_(-1)
			.numOutputBusChannels_(numChannels)
		);
		data = this.readSignal(tmp);

		File.delete(tmpFile);
		File.delete(tmp);
		^data

	}

	mirByFrame { |sound, analysisType, fftSize, numChannels = 1, args|
			var data;

			data = this.mir(sound, analysisType, fftSize, numChannels = 1, args);

		^data.clump((fftSize/server.options.blockSize).asInteger).collect{ |chunk| chunk.sum/chunk.size }

	}

	multiAnalysis { |sound, analysisTypes, fftSize, action|
		var cond = Condition.new, thisAction;
		{

			multiData = analysisTypes.collect{ |aTypes|
				var thisData;
				thisAction = {|data| cond.test_(true).signal; thisData = data };
				this.perform(aTypes[0], sound, fftSize: fftSize, args: aTypes[1], action: thisAction);
				cond.wait;
				thisData
			};
			action.value(multiData)
		}.forkIfNeeded
	}

	max { |data|
		var max;
		^(data.rank > 1).if({
			max = data.collect{ |dat| dat.maxItem }.maxItem;
			(max > 0).if({ data.collect{ |dat| dat/max} }, { data });
		}, {
			max = data.maxItem;
			(max > 0).if({ data/max }, { data })
		});
	}

	min { |data|
		^(data.rank > 1).if({
			data.collect{ |dat| dat - data.collect{ |dat| dat.minItem }.minItem }
		}, {
			data - data.minItem
		});
	}

	mfcc { |sound, numCoeffs = 13, fftSize = 1024, args|
		args !? { numCoeffs = args[0] };
		^this.mir(sound, MFCC, fftSize, numCoeffs, args ? [numCoeffs])
	}

	chromagram { |sound, numOctaveDivisions = 12, tuningBase = ('C0'.namecps), numOctaves = 8, integrationFlag = 0, integrationCoeff = 0.9, octaveRatio = 2, perFrameNormalize = 0, fftSize = 4096, hopSize, args|
		args !? { numOctaveDivisions = args[0]; args = [fftSize] ++ args };
		^this.mir(sound, Chromagram, fftSize, hopSize, numOctaveDivisions, args ? [fftSize, numOctaveDivisions, tuningBase, numOctaves, integrationFlag, integrationCoeff, octaveRatio, perFrameNormalize])
	}

	keyClarity { |sound, keyDecay = 2.0, chromaleak = 0.5, fftSize = 4096, hopSize, args|
		^this.mir(sound, KeyClarity, fftSize, hopSize, 1, args ? [keyDecay, chromaleak])
	}

	keyTrack { |sound, keyDecay = 2.0, chromaleak = 0.5, fftSize = 4096, hopSize, args|
		^this.mir(sound, KeyTrack, fftSize, hopSize, 1, args ? [keyDecay, chromaleak])
	}

	keyMode { |sound, keyDecay = 2.0, chromaleak = 0.5, fftSize = 4096, hopSize, args|
		^this.mir(sound, KeyMode, fftSize, hopSize, 1, args ? [keyDecay, chromaleak])
	}

	spectralEntropy { |sound, numBands = 9, fftSize = 2048, hopSize, args|
		args !? { numBands = args[0]; args = [fftSize] ++ args };
		^this.mir(sound, SpectralEntropy, fftSize, hopSize, numBands, args ? [fftSize, numBands])
	}

	tartini { |sound, threshold = 0.93, n = 2048, k = 0, overlap = 1024, smallCutoff = 0.5, args|
		^this.mir(sound, Tartini, nil, nil, 2, args ? [threshold, n, k, overlap, smallCutoff])
	}

	beatTrack { |sound, lock = 0, fftSize = 1024, hopSize, args|
		^this.mir(sound, BeatTrack, fftSize, hopSize, 4, args ? [lock])
	}

	loudness { |sound, smask = 0.25, tmask = 1, fftSize = 1024, hopSize, args|
		^this.mir(sound, Loudness, fftSize, hopSize, 1, args ? [smask, tmask])
	}

	sensoryDissonance { |sound, maxPeaks = 100, peakThreshold = 0.1, norm, clamp = 1.0, fftSize = 1024, hopSize, args|
		^this.mir(sound, SensoryDissonance, fftSize, hopSize, 1, args ? [maxPeaks, peakThreshold, norm, clamp])
	}

	specCentroid { |sound, fftSize = 2048, hopSize, args|
		^this.mir(sound, SpecCentroid, fftSize, hopSize, 1)
	}

	specPcile { |sound, fraction = 0.5, interpolate = 0, fftSize = 2048, hopSize, args|
		^this.mir(sound, SpecPcile, fftSize, hopSize, 1, args ? [fraction, interpolate])
	}

	specFlatness { |sound, fftSize = 2048, hopSize, args|
		^this.mir(sound, SpecFlatness, fftSize, hopSize, 1, nil)
	}

	fftCrest { |sound, freqLo = 0, freqHi = 50000, fftSize = 2048, hopSize, args|
		^this.mir(sound, FFTCrest, fftSize, hopSize, 1, args ? [freqLo, freqHi])
	}

	fftSpread { |sound, fftSize = 2048, hopSize, args|
		^this.mir(sound, FFTSpread, fftSize, hopSize, 1, nil)
	}

	fftSlope { |sound, fftSize = 2048, hopSize, args|
		^this.mir(sound, FFTSlope, fftSize, hopSize, 1, nil)
	}

	onsets { |sound, threshold = 0.5, odftype = 'rcomplex', relaxtime = 1, floor = 0.1, mingap = 10, medianspan = 11, whtype = 1, rawodf = 0, fftSize = 1024, hopSize, args|
		^this.mir(sound, Onsets, fftSize, hopSize, 1, args ? [threshold, odftype, relaxtime, floor, mingap, medianspan, whtype, rawodf])
	}

	duration { |sound|
		var data;
		this.checkForFilePath;
		SoundFile.use(soundFilePath, { |soundFile| data = soundFile.duration });
		^data
	}

	readSignal { arg path;
		var soundFile, soundFrames, soundEndFrame, endFrame, winSize, readSize, soundData;
		var overWriteIndex;

		if(File.exists(path), {

			// open...
			soundFile = SoundFile.new(path);
			soundFile.openRead;

			// read
			soundData = FloatArray.newClear(soundFile.numFrames * soundFile.numChannels);  // interleaved

			soundFile.readData(soundData);

			// multi-channel??
			(soundFile.numChannels > 1).if({
				soundData = soundData.clump(soundFile.numChannels).flop
			});
			// ... close
			soundFile.close;

			^soundData
		}, {
			Error("No sound file at:" + path).throw;
		})
	}

	fftPlot { |data|
		SpectrogramPlot.new(data: data).plot
	}

}