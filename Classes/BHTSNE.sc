BHTSNE {
	var <data, <mapPath, <initialDims, <perplexity, <maxIterations;
	var <command, <tmpPath, <pythonScript, <>twoDPath, <>threeDPath;

	*new { |data, mapPath, initialDims, perplexity, maxIterations|
		^super.newCopyArgs(data, mapPath, initialDims, perplexity, maxIterations).init
	}

	init {
		pythonScript = File.realpath(BHTSNE.filenameSymbol.asString.dirname ++ "/../run_bhtsne.py");
		tmpPath = PathName.tmp ++ 'bh_tsne.txt';
		(mapPath ++ "/tsne/").mkdir;
		twoDPath = mapPath ++ "/tsne/2d.tsv";
		twoDPath = mapPath ++ "/tsne/3d.tsv";
	}

	runCmd { |action, verbose = false|
		command = Python.pathToPython;
		command = command + pythonScript.escapeChar($ ) + data.escapeChar($ ) + twoDPath.escapeChar($ ) + threeDPath.escapeChar($ ) + initialDims + perplexity + maxIterations;
		verbose.if({ command.postln });
		command.unixCmd(action)
	}


}