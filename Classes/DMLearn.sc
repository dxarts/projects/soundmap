DMLearn {
	var <>dataMapView, numControls, <type, <window;
	var <learns, <>locs, <funcs, <txts, drawFunc, <isConnected, <centerLocks, centerColors;
	var <strings, <centerLearn, <>center, <centerConnected = false, centerString, centerTxt;
	var <centerButton, <buttons, <lockedButtons, <oscButton, <buttonStates, <lockedStates, <oscState = 0;
	var windowOffset = 400, xyTranslationFunc, <xyState = 0, <xyButton;

	*new { |dataMapView, numControls, type = (MIDILearn)|
		^super.newCopyArgs(dataMapView, numControls, type).init
	}

	init {
		centerLearn = OSCLearn.new;
		center = Cartesian.new;
		numControls.do{ |i| this.addlearn(i) };
		(type == OSCLearn).if({ windowOffset = windowOffset * 2 })
	}

	makeWindow {

		drawFunc = {
			locs.do{ |loc, i|

				isConnected[i].if({

					centerLocks[i].not.if({
						locs[i] = center
					}, {
						Pen.fillColor_(centerColors[i]);
						Pen.fillOval(Size(this.dataMapView.pixelSize, this.dataMapView.pixelSize).asRect
							.center_(this.dataMapView.translateToPixels(loc)));
					});
					strings[i] = locs[i].x.round(0.01).asString ++ "," + locs[i].y.round(0.01).asString ++ "," + learns[i].type ++ "," + learns[i].num;
					txts[i].string_(strings[i]);
				});

				centerString = this.dataMapView.center.x.round(0.01).asString ++ "," + this.dataMapView.center.y.round(0.01).asString;
				centerTxt.string_(centerString);

				centerConnected.if({
					this.dataMapView.center_(this.center);
				}, {
					this.center_(this.dataMapView.center)
				})


			}
		};

		this.dataMapView.dmParticles ?? {
			this.dataMapView.dmParticles_(DMParticles.new(this.dataMapView));
			this.dataMapView.dmParticles.makeWindow;
			this.dataMapView.dmParticles.window.front;
			this.dataMapView.pTxt.stringColor_(this.dataMapView.onCol);
		};
		window = Window.new(type.asString, Rect(windowOffset, Window.screenBounds.height, 400, 400));
		txts = numControls.collect{ |i| StaticText().string_(strings[i]) };
		centerString = "None";
		centerTxt = StaticText().string_(centerString);
		buttons = learns.collect{ |learn, index|

			Button.new().states_([
				["Learn", Color.black, Color.white],
				["Connect", Color.green, Color.white ],
				["Disconnect", Color.red, Color.white]
			])
			.action_({ |butt|
				buttonStates[index] = butt.value;
				case
				{ butt.value == 1 } {
					centerConnected.not.if({
						this.dataMapView.overrideMouseDownFunc(funcs[index])
					});
					learns[index].learn
				}
				{ butt.value == 2} {
					this.dataMapView.centerConnected.not.if({
						this.dataMapView.resumeMouseDownFunc;
						learns[index].connectTo({ this.dataMapView.addParticle(locs[index]) })
					}, {
						centerLocks[index].if({
							locs[index] = this.dataMapView.center;
							learns[index].connectTo({ this.dataMapView.addParticle(locs[index]) })
						}, {
							locs[index] = this.dataMapView.center;
							learns[index].connectTo({ this.dataMapView.addParticle(this.dataMapView.center) })
						})
					});
					learns[index].stop;
					isConnected[index] = true
				}
				{ butt.value == 0 } {
					learns[index].disconnect;
					isConnected[index] = false;
					strings[index] = "None";
					txts[index].string_(strings[index]);
				}
			})
		};
		lockedButtons = learns.collect{ |learn, index|
			Button().states_([
				["Center Locked", centerColors[index], Color.white],
				["Center Unlocked", Color.black, Color.white ]
			])
			.action_({ |butt|
				lockedStates[index] = butt.value;
				case
				{ butt.value == 1 } {
					centerLocks[index] = false;
					learns[index].disconnect;
					learns[index].connectTo({ this.dataMapView.addParticle(this.dataMapView.center) })
				}
				{ butt.value == 0} {
					centerLocks[index] = true;
					learns[index].disconnect;
					learns[index].connectTo({ this.dataMapView.addParticle(locs[index]) })
				}
			})
		};
		window.layout_(
			VLayout(
				HLayout(
					oscButton = Button.new().states_([
						["Learn OSC XY", Color.black, Color.white],
						["Enable OSC", Color.green, Color.white ],
						["Disconnect", Color.red, Color.white]
					])
					.action_({ |butt|
						oscState = butt.value;
						case
						{ butt.value == 1 } {
							centerLearn.learn
						}
						{ butt.value == 2} {
							centerConnected = true;
							this.dataMapView.centerConnected = true;
							centerLearn.stop;
							center = xyTranslationFunc.(centerLearn.msg[1..2]);
							centerLearn.connectTo({ |msg| center = xyTranslationFunc.(msg[1..2])  });
							this.dataMapView.refresh;

						}
						{ butt.value == 0 } {
							centerLearn.disconnect;
							this.dataMapView.centerConnected = false;
							centerConnected = false;
							centerString = "None";
							centerTxt.string_(centerString);
						}
					}),
					centerTxt
				),
				xyButton = Button.new().states_([
					["Using TouchOSC", Color.black, Color.white],
					["Using SuperColliderOSC", Color.black, Color.white ],
				])
				.action_({ |butt|
					xyState = butt.value;
					case
					{ butt.value == 0 } {
						xyTranslationFunc = { |xy| xy.reverse.linlin(0, 1, -1, 1).asCartesian}
					}
					{ butt.value == 1} {
						xyTranslationFunc = { |xy| xy.asCartesian}
					}
				}).valueAction_(0),
				*learns.collect{ |learn, i|
					HLayout(
						buttons[i],
						lockedButtons[i],
						txts[i]
					)
				}
			)
		);
		this.dataMapView.addDrawPointsFunc(drawFunc)
	}

	resetMIDIDevice{ |uid|
		learns.do{ |learn|
			learn.disconnect;
			learn.src_(uid);
		};
		this.connect(false)
	}

	resetOSCAddr { |aNetAddr|
		learns.do{ |learn|
			learn.disconnect;
			learn.addr_(aNetAddr);
		};
		centerLearn !? { centerLearn.disconnect; centerLearn.addr_(aNetAddr) };
		this.connect
	}

	particleVel { ^this.dataMapView.dmParticles.particleVel }

	connect { |connectOSC = true|
		learns.do{ |learn, index|
			isConnected[index].if({
				this.dataMapView.centerConnected.not.if({
					learn.connectTo({ this.dataMapView.addParticle(locs[index]) });
				}, {
					centerLocks[index].if({
						learns[index].connectTo({ this.dataMapView.addParticle(locs[index]) })
					}, {
						locs[index] = this.dataMapView.center;
						learns[index].connectTo({ this.dataMapView.addParticle(this.dataMapView.center) })
					})
				})
			})
		};
		(connectOSC and: centerConnected).if({
			centerLearn !? { centerLearn.connectTo({ |msg| center = msg[1..2].reverse.linlin(0, 1, -1, 1).asCartesian  }) }
		})
	}

	addlearn { |index|
		learns = learns.add(type.new);
		locs = locs.add(center);
		funcs = funcs.add({ |cart| locs[index] = cart });
		strings = strings.add("None");
		isConnected = isConnected.add(false);
		centerLocks = centerLocks.add(true);
		centerColors = centerColors.add(Color.rand);
		buttonStates = buttonStates.add(0);
		lockedStates = lockedStates.add(0);

	}

	saveSettings {
		^[centerConnected, [centerLearn.addr, centerLearn.path], oscState, xyState, this.saveLearns, locs, strings, isConnected, centerLocks, centerColors, buttonStates, lockedStates]
	}

	loadSettings { |settings|
		#centerConnected, centerLearn, oscState, xyState, learns, locs, strings, isConnected, centerLocks, centerColors, buttonStates, lockedStates = settings;
		centerLearn[0] !? { centerLearn = OSCLearn.new.addr_(centerLearn[0]).path_(centerLearn[1]) };
		this.loadLearns
	}

	saveLearns {
		^(this.type == MIDILearn).if({
			learns.collect{ |learn| [learn.type, learn.num, learn.src, learn.chan] }
		}, {
			learns.collect{ |learn| [learn.addr, learn.path] }
		})
	}

	loadLearns {
		learns = learns.collect{ |learn, index|
			isConnected[index].if({
				(this.type == MIDILearn).if({
					MIDILearn.new.type_(learn[0]).num_(learn[1]).src_(learn[2]).chan_(learn[3])
				}, {
					OSCLearn.new.addr_(learn[0]).path_(learn[1])
				})
			}, { type.new })
		}
	}

	updateButtonStates {
		buttonStates.do{ |state, i| buttons[i].value_(state) };
		lockedStates.do{ |state, i| lockedButtons[i].value_(state) };
		oscButton.value_(oscState);
		xyButton.valueAction_(xyState)
	}

	cleanup {
		learns.do(_.disconnect);
		this.dataMapView.removeDrawPointsFunc(drawFunc);
		window.close
	}

}