DMLearnSCOSC {
	var <>dataMapView, <window;
	var <learn, <>center, <connected = false, string, txt;
	var <oscButton, <oscState, <drawFunc;
	var windowOffset = 400;

	*new { |dataMapView|
		^super.newCopyArgs(dataMapView).init
	}

	init {
		learn = OSCLearn.new;
		windowOffset = windowOffset * 3;
		this.center_(Cartesian.new)
	}

	makeWindow {

		drawFunc = {

			string = this.dataMapView.center.x.round(0.01).asString ++ "," + this.dataMapView.center.y.round(0.01).asString;
			txt.string_(string);

			connected.if({
				this.dataMapView.center_(this.center);
			})


		};

		this.dataMapView.dmParticles ?? {
			this.dataMapView.dmParticles_(DMParticles.new(this.dataMapView));
			this.dataMapView.dmParticles.makeWindow;
			this.dataMapView.dmParticles.window.front;
			this.dataMapView.pTxt.stringColor_(this.dataMapView.onCol);
		};
		window = Window.new("SuperColliderOSC", Rect(windowOffset, Window.screenBounds.height, 400, 400));
		string = "None";
		txt = StaticText().string_(string);

		window.layout_(
			VLayout(
				HLayout(
					oscButton = Button.new().states_([
						["Learn OSC XY", Color.black, Color.white],
						["Enable OSC", Color.green, Color.white ],
						["Disconnect", Color.red, Color.white]
					])
					.action_({ |butt|
						oscState = butt.value;
						case
						{ butt.value == 1 } {
							learn.learn
						}
						{ butt.value == 2} {
							connected = true;
							this.dataMapView.centerConnected = true;
							learn.stop;

							learn.connectTo({ |msg|
								this.center_(msg[1..2].asCartesian);
								(this.dataMapView.particles.size < this.dataMapView.maxParticles).if({
									this.dataMapView.particles.addParticle(Particle.new(this.center, this.particleVel))
								})
							});

							this.dataMapView.refresh;

						}
						{ butt.value == 0 } {
							learn.disconnect;
							this.dataMapView.centerConnected = false;
							connected = false;
							string = "None";
							txt.string_(string);
						}
					}),
					txt
				)
			)
		);
		this.dataMapView.addDrawPointsFunc(drawFunc)
	}

	resetOSCAddr { |aNetAddr|


		learn !? { learn.disconnect; learn.addr_(aNetAddr) };
		this.connect
	}

	particleVel { ^this.dataMapView.dmParticles.particleVel }

	connect { |connectOSC = true|
		(connectOSC and: connected).if({
			learn !? { learn.connectTo({ |msg|
				this.center_(msg[1..2].asCartesian);
				(this.dataMapView.particles.size < this.dataMapView.maxParticles).if({
					this.dataMapView.particles.addParticle(Particle.new(this.center, this.particleVel))
				})
			})
			}
		})
	}


	saveSettings {
		^[connected, [learn.addr, learn.path], oscState, string]
	}

	loadSettings { |settings|
		#connected, learn, oscState, string = settings;
		learn[0] !? { learn = OSCLearn.new.addr_(learn[0]).path_(learn[1]) };

	}

	updateButtonStates {
		oscButton.value_(oscState)
	}

	cleanup {
		learn.disconnect;
		this.dataMapView.removeDrawPointsFunc(drawFunc);
		window.close
	}

}