DMParticles {
	var <>dataMap, <>playFunc, <>id;
	var <>particles;
	var <dmRhythm;
	var <>maxParticles = 50, <bounceFunc, forces, <>bounceVel = 1.0, <>startVel = 0.001;
	var <>playingState = 3, <>rhythmFunc;
	var <matchedParticles, <playingClick;
	var <>runFunc, <>dist, <>uniqueTrees, <>bounce = true;
	var <>stopFunc, <>xy, <>matchOnce = true;

	*new { |dataMap, playFunc, id|
		^super.newCopyArgs(dataMap, playFunc, id).init
	}

	init {
		particles = Particles.new;
		xy = Point.new;
		dmRhythm = DMRhythm.new(this, id);
		dmRhythm.playFunc = dmRhythm.playFunc.addFunc(playFunc);
		forces = Forces.new;
		matchedParticles = Array.newClear(this.dataMap.size);
		uniqueTrees = [];
		dist = 0.1;
		playingClick = IdentityDictionary();
		bounceFunc = { |kdtrees|

			// only try to play unique indices
			kdtrees.do{ |arr, i|

				// bounce off data points
				// only let particle bounce once per match

				forces.bounce(arr[2]); arr[2].velocity_(arr[2].velocity * this.bounceVel)

			}

		};
		runFunc = { |now|
			uniqueTrees = this.nearestParticles;
			this.bounce.if({ bounceFunc.(uniqueTrees) });
			this.playFunc.(uniqueTrees, id, now);
			this.particles.velocity_(this.startVel);
		}

	}


	run { this.particles.run }

	stopParticles {
		playingState !? {
			stopFunc.value;
			[{
				particles.stop;
				this.particles.removeRunFunc(runFunc)
			}, {
				particles.stop;
				this.particles.removeRunFunc(runFunc)
			}, {
				dmRhythm.stop;
				this.particles.removeRunFunc(rhythmFunc)
			}][playingState].value
		}
	}

	resetParticles { particles.removeAllParticles }

	handleParticles { |type|
		// only do something if changing state
		(type != playingState).if({
			this.stopParticles;
			[
				// particle behavior
				{
					this.run;
					this.particles.addRunFunc(runFunc);

				},
				// flock behavior
				{
					(this.particles.size > 30).if({ this.particles.particles_(this.particles.particles[0..29]) });
					this.particles.flock;
					this.particles.addRunFunc(runFunc);
				},
				// ryhthm behavior
				{
					this.resetParticles;
					dmRhythm.run;
					this.particles.addRunFunc(rhythmFunc);
				}
			][type].value;
			playingState = type;
		})
	}

	nearestParticles {
		var kdtrees, labels, index;
		this.uniqueTrees = [];
		// get the kdtree matches for each particle
		kdtrees = this.particles.particles.collect{ |particle|
			[this.dataMap.nearestKD(particle.location.asPoint), particle].flatten
		};

		kdtrees.notEmpty.if({
			// only get the matches within the desired distance, sort by distance
			kdtrees = kdtrees.sort{ |a, b| a[1] < b[1] };

			index = kdtrees.flop[1].indexOfGreaterThan(this.dist);

			index.notNil.if({
				kdtrees[index..].do{ |arr| arr[2].id_(nil) };
			}, {
				index = kdtrees.size
			});

			(index > 0).if({
				// iterate through the kdtrees
				kdtrees[0..index-1].do{ |arr, i|

					// if we don't previoulsy have a match with the same data point and playing particle
					(arr[0].label != arr[2].id).if({
						arr[2].id_(arr[0].label);
						uniqueTrees = uniqueTrees.add(arr)
					})

				}
			})
		});
		^this.uniqueTrees
	}

	clickPoints { |point, dist, click = true, id = 0|
		var kdtree, getTree, thisPlayingClick;
		this.uniqueTrees = [];

		// get the kdtree matches for each particle and sort them by distance
		kdtree = this.dataMap.nearestKD(point.asArray);

		getTree = {
			// if its a click, play as many times as you want
			(click or: this.matchOnce.not).if({ this.uniqueTrees = [kdtree] }, {
				// if it is a mouse move, play only once
				(playingClick[id] == kdtree[0].label).not.if({
					playingClick[id] = kdtree[0].label;
					this.uniqueTrees = [kdtree]
				})
			})
		};

		// if dist is nil, get closest
		dist.isNil.if({ getTree.value }, {
			// if not nil, only get if within dist
			(kdtree[1] < dist).if({ getTree.value }, { playingClick[id] = nil })
		});


		^this.uniqueTrees
	}

	nrt { |type, duration, mspeed, mforce, attraction, repulsion, cLocation, cForce, rhythmVel|
		[
			// particle behavior
			{
				this.particles.addRunFunc(runFunc);
				this.particles.nrt(0.08, duration)

			},
			// flock behavior
			{
				(this.particles.size > 30).if({ this.particles.particles_(this.particles.particles[0..29]) });
				this.particles.addRunFunc(runFunc);
				this.particles.flockNRT(0.08, duration, mspeed, mforce, attraction, repulsion, cLocation, cForce);

			},
			{
				this.dmRhythm.nrt(duration, rhythmVel)
			}
		][type].value;
	}


	particleVel { ^Spherical.new(this.startVel * 0.1.rrand(1.0), -pi.rrand(pi), -0.5pi.rrand(0.5pi)).asCartesian }

	addParticle { |cart, vel| (this.particles.size < this.maxParticles).if({  this.particles.addParticle(Particle.new(cart, vel ?? { this.particleVel })) }) }

	cleanup {
		this.stopParticles;
	}

	checkDirection { |point, particle| ^particle.location.dist(point) > (particle.location + particle.velocity).dist(point) }

	saveSettings {
		^[IdentityDictionary.new(know: true).putPairs([
			'particles.lifeRate', particles.lifeRate,
			'particles.maxspeed', particles.maxspeed,
			'particles.maxforce', particles.maxforce,
			'particles.attraction', particles.attraction,
			'particles.repulsion', particles.repulsion,
			'particles.centerLocation', particles.centerLocation,
			'particles.centerForce', particles.centerForce,
			'maxParticles', maxParticles,
			'bounceVel', bounceVel,
			'startVel', startVel,
			'dist', dist,
			'bounce', bounce,
			'playingState', playingState

		]),
		this.dmRhythm.saveSettings
		]
	}

	loadSettings { |settings|
		settings[0].keysValuesDo{ |key, val|
			(key == 'playingState').not.if({
				key.asString.contains(".").if({
					this.particles.perform(key.asString.splitext[1].asSymbol.asSetter, val)
				}, {
					this.perform(key.asSetter, val)
				})
			})
		};
		this.handleParticles(settings[0].playingState);
		this.dmRhythm.loadSettings(settings[1])
	}


}