DMParticlesView {
	var <dataMapView, <>dmParticles, <>dmSettings;
	var <>guiElements;
	var flockParams, flockControls;
	var <centerlocationX, <centerlocationY, flockFunc;
	var <getPointFunc, <getPointsFunc, <removePointFunc;
	var tempDist;

	*new { |dataMapView, dmParticles, dmSettings|
		^super.newCopyArgs(dataMapView, dmParticles, dmSettings).init
	}

	init {

		tempDist = 30.linlin(0, this.dataMapView.bounds.width, 0, 1) * 1.5;
		guiElements = IdentityDictionary.new(know: true);
		guiElements['nontoggle'] = IdentityDictionary.new(know: true);
		guiElements['toggle'] = IdentityDictionary.new(know: true);
		guiElements['slider'] = IdentityDictionary.new(know: true);

		this.makeFlockParams;

		getPointFunc = { |point|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.tempAddPoint = this.dmParticles.clickPoints(point, nil)[0][0].label
			})
		};

		getPointsFunc = { |point|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.tempAddPoint = this.dmParticles.dmRhythm.tempAddPoint.add(this.dmParticles.clickPoints(point, nil)[0][0].label)
			})
		};

		removePointFunc = { |point|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.tempRemovePoint = this.dmParticles.clickPoints(point, nil)[0][0].label
			})
		}
	}

	makeBehaviors {


		guiElements['nontoggle']['/particles/reset'] = Button.new(this.dataMapView.particleSettings).states_([
			["Reset Particles", Color.black, Color.white]
		])
		.action_({ |butt|
			this.dmParticles.resetParticles
		});

		guiElements['nontoggle']['/particles/addparticle'] = Button.new(this.dataMapView.particleSettings).states_([
			["Add Particle", Color.black, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState < 2).if({ this.dmParticles.addParticle(this.dmParticles.xy) })
		});

		guiElements['toggle']['/settings/dist'] = Button.new(this.dataMapView.particleSettings).states_([
			["Dist Enabled", Color.black, Color.white],
			["Dist Disabled", Color.black, Color.white],
		])
		.action_({ |butt|
			(this.dmParticles.playingState > 2).if({
				(butt.value == 1).if({
					this.dmParticles.dist = nil
				}, {
					this.dmParticles.dist = tempDist
				});
				this.updateTouchOSC
			}, {
				butt.value_(0)
			})
		});

		guiElements['nontoggle']['addParticleX'] = EZNumber(this.dataMapView.particleSettings, label: "X:", controlSpec: ControlSpec(-1.0, 1.0), action: { |ez|
			this.dmParticles.xy.x_(ez.value);
			this.dmSettings.guiElements['nontoggle']['addParticleX'] = ez.value
		}).value_(this.dmParticles.xy.x.round(0.01));
		guiElements['nontoggle']['addParticleY'] = EZNumber(this.dataMapView.particleSettings, label: "Y:", controlSpec: ControlSpec(-1.0, 1.0), action: { |ez|
			this.dmParticles.xy.y_(ez.value);
			this.dmSettings.guiElements['nontoggle']['addParticleY'] = ez.value
		}).value_(this.dmParticles.xy.y.round(0.01));


		['/settings/particles', '/settings/flock', '/settings/rhythm', '/settings/click'].do{ |type, inc|
			guiElements['nontoggle'][type] = Button.new(this.dataMapView.particleSettings).states_([
				[type.asString.basename.toUpper, Color.black, Color.white],
				[type.asString.basename.toUpper, Color.blue, Color.white]

			])
			.action_({ |butt|
				var states = ['/settings/particles', '/settings/flock', '/settings/rhythm', '/settings/click'];
				(butt.value == 1).if({
					(inc != 3).if({ this.dmParticles.dist = tempDist });
					(inc != 1).if({ this.closeOpenFlockFuncs }, { this.resumeOpenFlockFuncs });
					(inc != 2).if({ this.closeOpenRhythmFuncs }, { this.resumeOpenRhythmFuncs });
					this.dmParticles.handleParticles(inc);
					states.removeAt(inc);
					states.do{ | type| guiElements['nontoggle'][type].value_(0) };
				}, {
					(this.dmParticles.playingState == inc).if({
						butt.valueAction_(1)
					})
				});
				this.updateTouchOSC;
			})
		};


		^VLayout(
			HLayout(
				guiElements['nontoggle']['/settings/particles'],
				guiElements['nontoggle']['/settings/flock'],
				guiElements['nontoggle']['/settings/rhythm'],
				guiElements['nontoggle']['/settings/click']
			),
			HLayout(
				guiElements['nontoggle']['/particles/reset'],
				guiElements['toggle']['/settings/dist']
			),
			HLayout(
				guiElements['nontoggle']['/particles/addparticle'],
				guiElements['nontoggle']['addParticleX'].labelView,
				guiElements['nontoggle']['addParticleX'].numberView,
				guiElements['nontoggle']['addParticleY'].labelView,
				guiElements['nontoggle']['addParticleY'].numberView
			)
		)

	}

	makeSettings {

		guiElements['slider']['/settings/velocity'] = EZSlider(this.dataMapView.particleSettings, label: "Particle Velocity", controlSpec: ControlSpec(0.05, 2.0), action: { |thisSlider|
			this.dmParticles.startVel_(thisSlider.value/100);
			(this.dmParticles.playingState == 0).if({ this.dmParticles.particles.particles.do{ |particle| particle.setVel(thisSlider.value/100) } });
			(this.dmParticles.playingState == 1).if({ this.dmParticles.particles.frameRate_(thisSlider.value.reciprocal/20) });
			this.updateTouchOSC;
		}).value_(this.dmParticles.startVel*100);

		guiElements['slider']['/settings/liferate'] = EZSlider(this.dataMapView.particleSettings, label: "Life Rate", controlSpec: ControlSpec(0.0, 2.0, 'lin'), action: { |thisSlider|
			this.dmParticles.particles.lifeRate_(thisSlider.value.neg/100);
			this.updateTouchOSC;
		}).value_(this.dmParticles.particles.lifeRate.neg*100);

		guiElements['slider']['/settings/bouncevel'] = EZSlider(this.dataMapView.particleSettings, label: "Bounce Velocity", controlSpec: ControlSpec(0.1, 2.0, 'lin'), action: { |thisSlider|
			this.dmParticles.bounceVel_(thisSlider.value);
			this.updateTouchOSC;
		}).value_(this.dmParticles.bounceVel);

		guiElements['slider']['/settings/maxparticles'] = EZSlider(this.dataMapView.particleSettings, label: "Max Particles", controlSpec: ControlSpec(1, 500, 'lin'), action: { |thisSlider|
			this.dmParticles.maxParticles_(thisSlider.value);
			this.updateTouchOSC;
		}).value_(this.dmParticles.maxParticles);

		guiElements['slider']['/settings/frameRate'] = EZSlider(this.dataMapView.particleSettings, label: "Frame Rate", controlSpec: ControlSpec(0.04, 0.5, 'lin'), action: { |thisSlider|
			this.dmParticles.particles.frameRate_(thisSlider.value);
			this.updateTouchOSC;
		}).value_(this.dmParticles.particles.frameRate);

		guiElements['toggle']['/settings/bounce'] = Button.new(this.dataMapView.particleSettings).states_([
			["Enable Particle Bounce", Color.black, Color.white],
			["Stop Particle Bounce", Color.red, Color.white ]
		])
		.action_({ |butt|
			this.dmParticles.bounce = butt.value.asBoolean;
			this.updateTouchOSC;
		}).value_(this.dmParticles.bounce)
	}

	makeFlockParams {
		flockParams = Dictionary.new
		.putPairs([
			'maxspeed', 0.03,
			'maxforce', 0.01,
			'attraction', 0.1,
			'repulsion', 0.1,
			'centerForce', 0.5,
			'centerLocation', Cartesian.new,
		]);

		flockControls = flockParams.collect{ |val| val.isKindOf(Cartesian).not.if({ ControlSpec(val/10, val*10, 'exp') }) };
		flockControls['lifeRate'] = ControlSpec(0.0, 2.0, 'lin')
	}

	flockSettings {

		guiElements['slider']['/flock/maxforce'] = EZSlider(this.dataMapView.particleSettings, label: "Max Force", controlSpec: flockControls['maxforce'], action: { |thisSlider|
			this.updateTouchOSC;
			this.dmParticles.particles.maxforce_(thisSlider.value)
		}).valueAction_(this.dmParticles.particles.maxforce);

		guiElements['slider']['/flock/maxspeed'] = EZSlider(this.dataMapView.particleSettings, label: "Max Speed", controlSpec: flockControls['maxspeed'], action: { |thisSlider|
			this.updateTouchOSC;
			this.dmParticles.particles.maxspeed_(thisSlider.value)
		}).valueAction_(this.dmParticles.particles.maxspeed);

		guiElements['slider']['/flock/attraction'] = EZSlider(this.dataMapView.particleSettings, label: "Attraction", controlSpec: flockControls['attraction'], action: { |thisSlider|
			this.updateTouchOSC;
			this.dmParticles.particles.attraction_(thisSlider.value)
		}).valueAction_(this.dmParticles.particles.attraction);

		guiElements['slider']['/flock/repulsion'] = EZSlider(this.dataMapView.particleSettings, label: "Repulsion", controlSpec: flockControls['repulsion'], action: { |thisSlider|
			this.updateTouchOSC;
			this.dmParticles.particles.repulsion_(thisSlider.value)
		}).valueAction_(this.dmParticles.particles.repulsion);

		guiElements['slider']['/flock/centerforce'] = EZSlider(this.dataMapView.particleSettings, label: "Center Force", controlSpec: flockControls['centerForce'], action: { |thisSlider|
			this.updateTouchOSC;
			this.dmParticles.particles.centerForce_(thisSlider.value)
		}).valueAction_(this.dmParticles.particles.centerForce);

		flockFunc = { |point| this.dmParticles.particles.centerLocation_(point.asCartesian); this.dataMapView.refresh };

		centerlocationX = EZNumber(this.dataMapView.particleSettings, label: "X:", controlSpec: ControlSpec(-1.0, 1.0), action: { |ez| this.dmParticles.particles.centerLocation.x_(ez.value)
		}).value_(this.dmParticles.particles.centerLocation.x.round(0.01));
		centerlocationY = EZNumber(this.dataMapView.particleSettings, label: "Y:", controlSpec: ControlSpec(-1.0, 1.0), action: { |ez| this.dmParticles.particles.centerLocation.y_(ez.value)
		}).value_(this.dmParticles.particles.centerLocation.y.round(0.01));

		guiElements['toggle']['/flock/setFlockCenter'] = Button.new(this.dataMapView.particleSettings).states_([
			["Set Flock Center", Color.black, Color.white],
			["Stop", Color.red, Color.white ]
		])
		.action_({ |butt|
			this.dmSettings.guiElements['toggle']['/flock/setFlockCenter'] = butt.value;
			(butt.value == 1).if({
				this.dataMapView.overrideMouseDownFunc(flockFunc);
				this.dataMapView.overrideMouseMoveFunc(flockFunc);
			}, {
				this.dataMapView.resumeMouseDownFunc;
				this.dataMapView.resumeMouseMoveFunc;
				centerlocationX.value_(this.dmParticles.particles.centerLocation.x.round(0.01));
				centerlocationY.value_(this.dmParticles.particles.centerLocation.y.round(0.01));
			})
		});

		^VLayout(
			HLayout(
				guiElements['slider']['/flock/maxforce'].labelView,
				guiElements['slider']['/flock/maxforce'].sliderView,
				guiElements['slider']['/flock/maxforce'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/flock/maxspeed'].labelView,
				guiElements['slider']['/flock/maxspeed'].sliderView,
				guiElements['slider']['/flock/maxspeed'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/flock/attraction'].labelView,
				guiElements['slider']['/flock/attraction'].sliderView,
				guiElements['slider']['/flock/attraction'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/flock/repulsion'].labelView,
				guiElements['slider']['/flock/repulsion'].sliderView,
				guiElements['slider']['/flock/repulsion'].numberView.maxWidth_(40),
			),
			HLayout(
				guiElements['slider']['/flock/centerforce'].labelView,
				guiElements['slider']['/flock/centerforce'].sliderView,
				guiElements['slider']['/flock/centerforce'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['toggle']['/flock/setFlockCenter'],
				HLayout(centerlocationX.labelView, centerlocationX.numberView),
				HLayout(centerlocationY.labelView, centerlocationY.numberView)
			)


		)
	}

	rhythmView {
		guiElements['toggle']['/rhythm/addpoint'] = Button.new(this.dataMapView.particleSettings).states_([
			["Add Point", Color.black, Color.white],
			["Done!", Color.red, Color.white ]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmSettings.guiElements['toggle']['/rhythm/addpoint'] = butt.value;
				(butt.value == 1).if({
					this.dataMapView.overrideMouseDownFunc(this.getPointFunc);
					this.dataMapView.overrideMouseMoveFunc(this.getPointFunc);
				}, {
					this.dataMapView.resumeMouseDownFunc;
					this.dataMapView.resumeMouseMoveFunc;
					// protect against adding nil
					this.dmParticles.dmRhythm.isPlaying.not.if({
						this.dmParticles.dmRhythm.tempAddPoint !? { this.dmParticles.dmRhythm.points = this.dmParticles.dmRhythm.points.add(this.dmParticles.dmRhythm.tempAddPoint); this.dmParticles.dmRhythm.tempAddPoint = nil; }
					}, {
						this.dmParticles.dmRhythm.addPointSync(this.dmParticles.dmRhythm.tempAddPoint)
					});
				});
				this.updateTouchOSC
			})
		});

		guiElements['toggle']['/rhythm/addpoints'] = Button.new(this.dataMapView.particleSettings).states_([
			["Add Points", Color.black, Color.white],
			["Done!", Color.red, Color.white ]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmSettings.guiElements['toggle']['/rhythm/addpoints'] = butt.value;
				(butt.value == 1).if({
					this.dataMapView.overrideMouseDownFunc(this.getPointsFunc);
					this.dataMapView.overrideMouseMoveFunc(this.getPointsFunc);
				}, {
					this.dataMapView.resumeMouseDownFunc;
					this.dataMapView.resumeMouseMoveFunc;
					// protect against adding nil
					this.dmParticles.dmRhythm.isPlaying.not.if({
						this.dmParticles.dmRhythm.tempAddPoint !? {
							this.dmParticles.dmRhythm.tempAddPoint.isKindOf(Array).if({ this.dmParticles.dmRhythm.tempAddPoint = this.removeDuplicates(this.dmParticles.dmRhythm.tempAddPoint) });
							this.dmParticles.dmRhythm.points = this.dmParticles.dmRhythm.points.add(this.dmParticles.dmRhythm.tempAddPoint).flatten;
							this.dmParticles.dmRhythm.tempAddPoint = nil
						}
					}, {
						this.dmParticles.dmRhythm.tempAddPoint.isKindOf(Array).if({ this.dmParticles.dmRhythm.tempAddPoint = this.removeDuplicates(this.dmParticles.dmRhythm.tempAddPoint) });
						this.dmParticles.dmRhythm.addPointSync(this.dmParticles.dmRhythm.tempAddPoint)
					});
				});
				this.updateTouchOSC
			})
		});

		guiElements['nontoggle']['/rhythm/randomize'] = Button.new(this.dataMapView.particleSettings).states_([
			["Randomize", Color.black, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.randomize
			})
		});

		guiElements['nontoggle']['/rhythm/return'] = Button.new(this.dataMapView.particleSettings).states_([
			["Return", Color.black, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.derandomize; guiElements['toggle']['/rhythm/heap'].value_(0)
			})
		});

		guiElements['toggle']['/rhythm/heap'] = Button.new(this.dataMapView.particleSettings).states_([
			["Heap", Color.black, Color.white],
			["Heap", Color.red, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmParticles.dmRhythm.heap = butt.value.asBoolean; this.updateTouchOSC
			})
		});

		guiElements['toggle']['/rhythm/removepoint'] = Button.new(this.dataMapView.particleSettings).states_([
			["Remove Point", Color.black, Color.white],
			["Done!", Color.red, Color.white ]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				this.dmSettings.guiElements['toggle']['/rhythm/removepoint'] = butt.value;
				this.dmParticles.dmRhythm.points.notEmpty.if({
					(butt.value == 1).if({
						this.dataMapView.overrideMouseDownFunc(this.removePointFunc);
						this.dataMapView.overrideMouseMoveFunc(this.removePointFunc);
					}, {
						this.dataMapView.resumeMouseDownFunc;
						this.dataMapView.resumeMouseMoveFunc;
						this.dmParticles.dmRhythm.tempRemovePoint !? {
							this.dmParticles.dmRhythm.points.includes(this.dmParticles.dmRhythm.tempRemovePoint).if({
								this.dmParticles.dmRhythm.isPlaying.not.if({
									this.dmParticles.dmRhythm.points.remove(this.dmParticles.dmRhythm.tempRemovePoint); this.dmParticles.dmRhythm.tempRemovePoint = nil
								}, {
									this.dmParticles.dmRhythm.removePointSync(this.dmParticles.dmRhythm.tempRemovePoint)
								})
							}, {
								("Point: " ++ this.dmParticles.dmRhythm.tempRemovePoint + "not in points").warn
							})
						}
					});
				}, {
					butt.value_(0)
				});
				this.updateTouchOSC
			})
		});

		guiElements['nontoggle']['/rhythm/reset'] = Button.new(this.dataMapView.particleSettings).states_([
			["Reset", Color.black, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({ this.dmParticles.dmRhythm.reset })
		});

		guiElements['nontoggle']['/rhythm/clear'] = Button.new(this.dataMapView.particleSettings).states_([
			["Clear", Color.black, Color.white]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({ this.dmParticles.dmRhythm.clear; guiElements['toggle']['/rhythm/play'].value_(0); this.updateTouchOSC })
		});

		guiElements['toggle']['/rhythm/play'] = Button.new(this.dataMapView.particleSettings).states_([
			["Play", Color.black, Color.white],
			["Stop", Color.red, Color.white ]
		])
		.action_({ |butt|
			(this.dmParticles.playingState == 2).if({
				(butt.value == 1).if({
					this.dmParticles.dmRhythm.play
				}, {
					this.dmParticles.dmRhythm.stop
				})
			});
			butt.value_(this.dmParticles.dmRhythm.isPlaying.asInteger);
			this.updateTouchOSC;
		});


		^VLayout(
			guiElements['toggle']['/rhythm/play'],
			guiElements['toggle']['/rhythm/addpoint'],
			guiElements['toggle']['/rhythm/addpoints'],
			guiElements['toggle']['/rhythm/removepoint'],
			guiElements['nontoggle']['/rhythm/randomize'],
			guiElements['nontoggle']['/rhythm/return'],
			guiElements['toggle']['/rhythm/heap'],
			guiElements['nontoggle']['/rhythm/clear'],
			guiElements['nontoggle']['/rhythm/reset']
		)

	}


	layOut {



		this.makeSettings;

		^VLayout(
			this.makeBehaviors,
			HLayout(
				guiElements['slider']['/settings/velocity'].labelView,
				guiElements['slider']['/settings/velocity'].sliderView,
				guiElements['slider']['/settings/velocity'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/settings/liferate'].labelView,
				guiElements['slider']['/settings/liferate'].sliderView,
				guiElements['slider']['/settings/liferate'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/settings/bouncevel'].labelView,
				guiElements['slider']['/settings/bouncevel'].sliderView,
				guiElements['slider']['/settings/bouncevel'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/settings/maxparticles'].labelView,
				guiElements['slider']['/settings/maxparticles'].sliderView,
				guiElements['slider']['/settings/maxparticles'].numberView.maxWidth_(40)
			),
			HLayout(
				guiElements['slider']['/settings/frameRate'].labelView,
				guiElements['slider']['/settings/frameRate'].sliderView,
				guiElements['slider']['/settings/frameRate'].numberView.maxWidth_(40)
			),
			guiElements['toggle']['/settings/bounce'],
			this.flockSettings,
			this.rhythmView
		)

	}


	saveSettingsArchive {
		^[this.guiElements['nontoggle'].collect{ |element| element.value }, this.guiElements['toggle'].collect{ |element| element.value }, this.guiElements['slider'].collect{ |element| element.value }, this.playingState, this.id, this.dmRhythm.points, this.dmRhythm.inc, this.particles.centerLocation]
	}

	loadSettingsArchive { |settings|
		// this.playingState = settings[1];
		this.id = settings[4];
		this.dmRhythm.points = settings[5];
		this.dmRhythm.inc = settings[6];
		this.particles.centerLocation = settings[7];
		this.updateGUIElements(settings[0], settings[1], settings[2]);
	}

	saveSettingsView {
		^[this.dmParticles.saveSettings, this.dmRhythm.saveSettings]
	}

	loadSettingsView { |settings|
		this.loadSettings(settings[0]);
		this.dmRhythm.loadSettings(settings[1]);
		settings.do{ |setting|
			this.updateGUIElements(setting)
		}
	}

	updateGUIElementsArchive { |nontoggleSettings, toggleSettings, sliderSettings|
		nontoggleSettings.keysValuesDo{ |key, value|
			['/settings/reset', '/rhythm/reset', '/particles/addparticle'].includes(key).not.if({
				guiElements['nontoggle'][key].valueAction_(value)
			})
		};
		toggleSettings.keysValuesDo{ |key, value|
			['/rhythm/addpoint', '/rhythm/play', '/rhythm/removepoint', '/rhythm/addpoints'].includes(key).not.if({
				guiElements['toggle'][key].valueAction_(value)
			})
		};

		sliderSettings.keysValuesDo{ |key, value|
			guiElements['slider'][key].valueAction_(value)
		}
	}

	updateGUIElements {
		var states = ['/settings/particles', '/settings/flock', '/settings/rhythm', '/settings/click'];
		this.dmParticles.handleParticles(this.dmParticles.playingState);
		guiElements['nontoggle'][states.removeAt(this.dmParticles.playingState)].value_(1);
		states.do{ | type| guiElements['nontoggle'][type].value_(0) };
		guiElements['slider']['/settings/velocity'].value_(this.dmParticles.startVel*100);
		guiElements['slider']['/settings/liferate'].value_(this.dmParticles.particles.lifeRate.neg*100);
		guiElements['slider']['/settings/bouncevel'].value_(this.dmParticles.bounceVel);
		guiElements['slider']['/settings/maxparticles'].value_(this.dmParticles.maxParticles);
		guiElements['slider']['/flock/maxforce'].value_(this.dmParticles.particles.maxforce);
		guiElements['slider']['/flock/maxspeed'].value_(this.dmParticles.particles.maxspeed);
		guiElements['slider']['/flock/attraction'].value_(this.dmParticles.particles.attraction);
		guiElements['slider']['/flock/repulsion'].value_(this.dmParticles.particles.repulsion);
		guiElements['slider']['/flock/centerforce'].value_(this.dmParticles.particles.centerForce);
		guiElements['toggle']['/settings/bounce'].value_(this.dmParticles.bounce.asInteger);
		centerlocationX.value_(this.dmParticles.particles.centerLocation.x);
		centerlocationY.value_(this.dmParticles.particles.centerLocation.y);
		guiElements['toggle']['/rhythm/heap'].value_(this.dmParticles.dmRhythm.heap.asInteger);
		guiElements['toggle']['/rhythm/play'].value_(this.dmParticles.dmRhythm.isPlaying.asInteger);
		guiElements['toggle']['/settings/dist'].value_(this.dmParticles.dist.isNil.asInteger);
		this.resumeOpenFuncs;

	}

	closeOpenFuncs {
		this.closeOpenFlockFuncs;
		this.closeOpenRhythmFuncs;
	}

	closeOpenFlockFuncs {
		(guiElements['toggle']['/flock/setFlockCenter'].value == 1).if({ guiElements['toggle']['/flock/setFlockCenter'].valueAction_(0) })
	}

	closeOpenRhythmFuncs {
		[
			guiElements['toggle']['/rhythm/addpoint'],
			guiElements['toggle']['/rhythm/addpoints'],

			guiElements['toggle']['/rhythm/removepoint']
		].do{ |guiEle|
			(guiEle.value == 1).if({ guiEle.valueAction_(0) })
		}
	}

	resumeOpenFuncs {
		this.resumeOpenRhythmFuncs;
		this.resumeOpenFlockFuncs
	}

	resumeOpenRhythmFuncs {
		['/rhythm/addpoint', '/rhythm/addpoints', '/rhythm/removepoint'].do{ |settingEle|
			(this.dmSettings.guiElements['toggle'][settingEle] == 1).if({
				guiElements['toggle'].valueAction_(1)
			})
		}
	}

	resumeOpenFlockFuncs {
		(this.dmSettings.guiElements['toggle']['/flock/setFlockCenter'] == 1).if({ guiElements['toggle']['/flock/setFlockCenter'].valueAction_(1) })
	}

	updateTouchOSC {
		this.dataMapView.touchOSC !? { this.dataMapView.touchOSC.updateTouchOSC }
	}

	removeDuplicates { |arr|
		var res = [];
		arr.do{ |item| res.includes(item).not.if({ res = res.add(item) }) };
		^res
	}


}