DMParticlesViewSettings {
	var <guiElements;

	*new {
		^super.new.init
	}

	init {
		guiElements = IdentityDictionary.new(know: true);
		guiElements['nontoggle'] = IdentityDictionary.new(know: true);
		guiElements['toggle'] = IdentityDictionary.new(know: true);
		guiElements['slider'] = IdentityDictionary.new(know: true);
		guiElements['toggle']['/rhythm/addpoint'] = 0;
		guiElements['toggle']['/rhythm/addpoints'] = 0;
		guiElements['toggle']['/flock/setFlockCenter'] = 0;
		guiElements['toggle']['/rhythm/removepoint'] = 0;
		guiElements['nontoggle']['addParticleX'] = 0.0;
		guiElements['nontoggle']['addParticleY'] = 0.0;
	}
}