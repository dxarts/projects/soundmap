DMRhythm {
	var <>particleSettings, <>id, <>playFunc;
	var <>points, <>inc = 0, <playTask;
	var <>tempAddPoint, <>tempRemovePoint;
	var <>addPoint = false, <>removePoint = false, <>isPlaying = false;
	var <>stopFunc, <>scramble = false, <>heap = false, <>scrambledPoints, <>heapPoints;

	*new { |particleSettings, id|
		^super.newCopyArgs(particleSettings, id).init
	}

	init {
		points = [];
		playTask = Task({
			var particle, timer = 0.0, currentPoints;
			loop{
				(timer <= 0.0).if({
					timer = this.getDist(inc);

					particle = this.particleSettings.particles.particles[0];
					particle.notNil.if({
						particle.location_(this.getCenter);
						particle.velocity_(Cartesian.new);
						(heap and: (inc.mod(points.size) == 0)).if({ heapPoints = points.scramble });
						currentPoints = case
						{ heap and: heapPoints.notNil } { heapPoints }
						{ scramble and: scrambledPoints.notNil } { scrambledPoints }
						{ points };
						playFunc.value([[KDTree([[0,0,0, currentPoints.wrapAt(inc)]], lastIsLabel:true)]], id, timer * 0.1/0.08);
						inc = inc + 1;
					}, {
						stopFunc.value;
						playTask.stop;
					});

				}, {
					timer = timer - (this.particleSettings.startVel * 20)
				});
				addPoint.if({
					tempAddPoint !? {
						tempAddPoint.isKindOf(Array).not.if({ tempAddPoint = [tempAddPoint] });
						tempAddPoint.do{ |point|
							points = points.add(point);
							scrambledPoints = scrambledPoints.add(point)
						}
					};
					addPoint = false;
					tempAddPoint = nil;

				});
				removePoint.if({ tempRemovePoint !? { points.remove(tempRemovePoint); scrambledPoints.remove(tempRemovePoint) }; removePoint = false; tempRemovePoint = nil; });
				0.08.wait;
			}
		})

	}

	run { this.particleSettings.run(0.08) }

	play {
		(points.notEmpty and: isPlaying.not).if({
			isPlaying = true;
			this.particleSettings.addParticle(this.getPointLoc(inc), this.getParticleVel(this.getPointLoc(inc), inc));
			playTask.play
		})
	}

	stop {
		isPlaying = false;
		playTask.stop;
		playTask.reset;
		this.particleSettings.particles.removeParticle(this.particleSettings.particles.particles[0])
	}

	getPointLoc { |index|
		^this.getLoc(points.wrapAt(index))
	}

	getLoc { |point|
		^this.particleSettings.dataMap.data[point]
	}

	getDist { |index|
		^this.getPointLoc(index + 1).dist(this.getPointLoc(index))
	}

	getParticleVel { |cart, index|
		cart = this.getPointLoc(index + 1).asCartesian - cart;
		^cart.asSpherical.rho_(this.particleSettings.startVel * 10).asCartesian
	}

	addPointSync { |point| addPoint = true; tempAddPoint = point }
	removePointSync { |point| removePoint = true; tempRemovePoint = point }

	nrt { |duration, vel|
		var now = 0.0, particle, timer = 0.0;
			while({ now < duration }, {
				(timer <= 0.0).if({
					timer = this.getDist(inc);

					particle = this.particleSettings.particles.particles[0];
						playFunc.value([[KDTree([[0,0,0, points.wrapAt(inc)]], lastIsLabel:true)]], id, now);
						inc = inc + 1;

				}, {
					timer = timer - vel[now]
				});
				addPoint.if({
					tempAddPoint !? {
						tempAddPoint.isKindOf(Array).not.if({ tempAddPoint = [tempAddPoint] });
						tempAddPoint.do{ |point|
							points = points.add(point)
						}
					};
					addPoint = false;
					tempAddPoint = nil;

				});
				removePoint.if({ tempRemovePoint !? { points.remove(tempRemovePoint) }; removePoint = false; tempRemovePoint = nil; });
				now = now + 0.01;
		})
	}

	randomize { this.scramble = true; scrambledPoints = points.scramble }
	derandomize { this.scramble = false; this.heap = false }

	getCenter { ^points.collect{ |point| this.getLoc(point) }.sum/points.size }

	reset { inc = 0 }
	clear { this.stop; this.points = []; inc = 0; this.scramble = false }

	saveSettings {
		^Dictionary.new.putPairs([
			'points', points,
			'scrambledPoints', scrambledPoints,
			'heapPoints', heapPoints,
			'inc', inc,
			'heap', heap,
			'scramble', scramble
		])
	}

	loadSettings { |settings|
		settings.keysValuesDo{ |key, val|
			this.perform(key.asSetter, val)
		}
	}


}