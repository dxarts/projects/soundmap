DMTouchOSC {
	var <>dataMapView, <>dmParticles;
	var <learn, <funcs;
	var <button, <addrText, <portText, <play, <record, <buttonState;
	var bounceState = 0;
	var draw = false, xyFuncs, addFunc, timers, removeTasks, <points;
	var <oscMultiRecorder, <oscTrack, <playTasks, <paths, sortDict;

	*new { |dataMapView, dmParticles|
		^super.newCopyArgs(dataMapView, dmParticles).init
	}

	init {
		funcs = [];
		learn = OSCLearn.new;
		timers = Array.fill(5, { 1 });
		points = Array.newClear(6);
		xyFuncs = Array.fill(6, { |inc|
			{
				this.points[inc].notNil.if({
					Pen.strokeColor_(Color.black);
					Pen.line(this.dataMapView.translateToPixels(Point(-1, this.points[inc].y)), this.dataMapView.translateToPixels(Point(1, this.points[inc].y)));
					Pen.line(this.dataMapView.translateToPixels(Point(this.points[inc].x, -1)), this.dataMapView.translateToPixels(Point(this.points[inc].x,1)));
					Pen.stroke
				})
			}
		});
		xyFuncs.do{ |func| this.dataMapView.addDrawPointsFunc(func) };
		removeTasks = Array.fill(5, { |inc|
			Task({
				loop {
					(timers[inc] > 0).if({
						timers[inc] = timers[inc] - 0.01;
					}, {
						this.points[inc] = nil;
						{ this.dataMapView.refresh }.defer;
						removeTasks[inc].stop
					});
					0.01.wait
				}
			})
		});

		sortDict = Dictionary.new;

		this.dataMapView.particlesView.guiElements.keysValuesDo{ |key, dict|
			dict.keys.do{ |key2|
				sortDict[key2] = key;
				paths = paths.add(key2);
			}
		};

		8.do{ |inc|
			var mod;
			mod = (inc < 4).if({ 2 }, { 1 });
			paths = paths.add(('/settings/enable/' ++ mod ++ '/' ++ (inc.mod(4) + 1)).asSymbol)
		};

		5.do{ |inc|
			paths = paths.add(('/soundMap/multixy/' ++ (inc + 1)).asSymbol)
		};

		paths = paths.add('/rhythm/xy3');

		paths = paths.add('/particles/xy');

		paths = paths.add('/particles/clear');

		paths = paths.add('/flock/flockcenter');

		oscMultiRecorder = OSCRecorder(paths);

		this.makeGUI

	}

	makeGUI {


		button = Button.new().states_([
			["Learn", Color.black, Color.white],
			["Connect", Color.green, Color.white ],
			["Disconnect", Color.red, Color.white]
		])
		.action_({ |butt|
			buttonState = butt.value;
			case
			{ butt.value == 1 } {
				learn.reset;
				learn.learn
			}
			{ butt.value == 2 } {
				learn.stop;
				learn.addr.notNil.if({
					funcs.notEmpty.if({ this.disconnect; "disconnected".postln });
					this.connect;
					this.setAddrText
				}, {
					"Connection unsuccessful".warn;
					("SC lang port is: " ++ NetAddr.langPort).postln;
					butt.value_(0)
				});
			}
			{ butt.value == 0 } {
				this.disconnect;
			}
		});

		record = Button.new().states_([
			["Record", Color.black, Color.white],
			["Stop", Color.red, Color.white ]
		])
		.action_({ |butt|
			case
			{ butt.value == 1 } {
				oscMultiRecorder.record
			}
			{ butt.value == 0} {
				oscMultiRecorder.stop;
				Dialog.savePanel({ |path|
					oscMultiRecorder.oscTrack.writeArchive(path) },
				path: Platform.userAppSupportDir);
			}

		});

		play = Button.new().states_([
			["Load", Color.black, Color.white],
			["Play", Color.green, Color.white ],
			["Stop", Color.red, Color.white ],
		])
		.action_({ |butt|
			case
			{ butt.value == 1 } {
				Dialog.openPanel({ |path|
					oscTrack = Object.readArchive(path) },
				path: Platform.userAppSupportDir);
			}
			{ butt.value == 2} {
				learn.addr.isNil.if({ button.valueAction_(2) });
				this.connect;
				playTasks =
				Task({
					var now = 0.0;
					oscTrack.events.do{ |oscEvent|
						(oscEvent.time - now).wait;
						now = oscEvent.time;
						NetAddr.localAddr.sendMsg(oscEvent.path, *oscEvent.val)
					}
				});
				playTasks.play
			}
			{ butt.value == 0 } {
				playTasks.stop
			}

		});

		addrText = EZText(this.dataMapView.particleSettings, label:"IP", action: { |ez| learn.addr !? { learn.addr.hostname_(ez.value); this.disconnect; this.connect } }, initVal: learn.addr.notNil.if({ learn.addr.ip }, { "None" }));

		portText = EZText(this.dataMapView.particleSettings, label:"Port", action: { |ez| learn.addr !? { learn.addr.port_(ez.value); this.disconnect; this.connect } }, initVal: learn.addr.notNil.if({ learn.addr.port }, { "None" }));

	}

	makeWindow {
		^HLayout(
			button,
			HLayout(
				addrText.labelView,
				addrText.textField
			),
			HLayout(
				portText.labelView,
				portText.textField
			),
			record,
			play
		)
	}

	setAddrText { addrText.textField.string_("\"" ++ learn.addr.ip ++ "\""); portText.textField.string_(learn.addr.port) }

	connect {
		this.disconnect;
		this.settings;
		this.dmFlock;
		this.dmRhythm;
		5.do{ |inc|
			funcs = funcs.add(
				OSCFunc({ |msg|
					var point;
					point = msg[1..2].asCartesian;
					case
					{ (this.dmParticles.playingState == 3) } {
						this.dmParticles.playFunc.(this.dmParticles.clickPoints(point.asPoint, this.dmParticles.dist, false, inc), this.dataMapView.dmParticleID);
						{ this.dataMapView.refresh }.defer
					}
					{ (this.dmParticles.playingState == 0) or: (this.dmParticles.playingState == 1) }
					{
						this.dmParticles.addParticle(point)
					};
					this.points[inc] = point;
					{ this.dataMapView.refresh }.defer;
					timers[inc] = 0.2;
					removeTasks[inc].isPlaying.not.if({ removeTasks[inc].start });
				}, ('/soundMap/multixy/' ++ (inc + 1)).asSymbol, learn.addr)
			)
		};

		// TO DO fix
		8.do{ |inc|
			var mod;
			mod = (inc < 4).if({ 2 }, { 1 });
			funcs = funcs.add(OSCFunc({ |msg| {
				this.dataMapView.enableButtons[inc].valueAction_(1);
			}.defer }, ('/settings/enable/' ++ mod ++ '/' ++ (inc.mod(4) + 1)).asSymbol, learn.addr))
		};

		funcs = funcs.add(OSCFunc({ |msg|
			var point;
			point = msg[1..2].asPoint;
			{
				this.dataMapView.particlesView.guiElements['nontoggle']['addParticleX'].valueAction_(point.x);
				this.dataMapView.particlesView.guiElements['nontoggle']['addParticleY'].valueAction_(point.y);
				this.points[5] = point;
				{ this.dataMapView.refresh }.defer;
			}.defer

		}, '/particles/xy', learn.addr));

		funcs = funcs.add(OSCFunc({ |msg| this.points[5] = nil }, '/particles/clear', learn.addr));

		this.updateTouchOSC;

	}

	settings {

		this.dataMapView.particlesView.guiElements['nontoggle'].keysValuesDo{ |key, button|

			funcs = funcs.add(OSCFunc({ |msg|
				{ this.dataMapView.particlesView.guiElements['nontoggle'][key].valueAction_(1) }.defer
			}, key, learn.addr))

		};

		this.dataMapView.particlesView.guiElements['slider'].keysValuesDo{ |key, slider|

			funcs = funcs.add(OSCFunc({ |msg|
				{ this.dataMapView.particlesView.guiElements['slider'][key].valueAction_(slider.controlSpec.map(msg[1])) }.defer
			}, key, learn.addr))
		};

		this.dataMapView.particlesView.guiElements['toggle'].keysValuesDo{ |key, button|
			funcs = funcs.add(OSCFunc({ |msg|
				{ this.dataMapView.particlesView.guiElements['toggle'][key].valueAction_(this.dataMapView.particlesView.guiElements['toggle'][key].value.asBoolean.not.asInteger) }.defer;
			}, key, learn.addr))
		};

		funcs = funcs.add(OSCFunc({ |msg| { this.dataMapView.particlesView.dataMapView.masterReset.valueAction_(1) }.defer }, '/settings/resetall', learn.addr));
	}

	dmFlock {

		funcs = funcs.add(OSCFunc({ |msg|
			var point;
			point = msg[1..2].asCartesian;
			this.dmParticles.particles.centerLocation_(point)
		}, '/flock/flockcenter', learn.addr));
	}

	dmRhythm {

		funcs = funcs.add(OSCFunc({ |msg|
			{
				(this.dataMapView.particlesView.guiElements['toggle']['/rhythm/addpoint'].value == 1).if({
					this.dataMapView.particlesView.getPointFunc.(msg[1..2].reverse.linlin(0, 1, -1, 1).asPoint)
				});
				(this.dataMapView.particlesView.guiElements['toggle']['/rhythm/addpoints'].value == 1).if({
					this.dataMapView.particlesView.getPointsFunc.(msg[1..2].reverse.linlin(0, 1, -1, 1).asPoint)
				});
				(this.dataMapView.particlesView.guiElements['toggle']['/rhythm/removepoint'].value == 1).if({
					this.dataMapView.particlesView.removePointFunc.(msg[1..2].reverse.linlin(0, 1, -1, 1).asPoint)
				})
			}.defer
		}, '/rhythm/xy3', learn.addr));
	}

	disconnect { funcs.do{ |oscFunc| oscFunc.free }; funcs = [] }

	updateTouchOSC {

		learn.addr.notNil.if({

			this.dataMapView.particlesView.guiElements['nontoggle'].keysValuesDo{ |key, button|
				learn.addr.sendMsg(key, this.dataMapView.particlesView.guiElements['nontoggle'][key].value)
			};

			this.dataMapView.particlesView.guiElements['slider'].keysValuesDo{ |key, slider|
				learn.addr.sendMsg(key, slider.controlSpec.unmap(slider.value))
			};

			this.dataMapView.particlesView.guiElements['toggle'].keysValuesDo{ |key, button|
				learn.addr.sendMsg(key, this.dataMapView.particlesView.guiElements['toggle'][key].value)
			};

			8.do{ |inc|
				var mod;
				mod = (inc < 4).if({ 2 }, { 1 });
				learn.addr.sendMsg(('/settings/enable/' ++ mod ++ '/' ++ (inc.mod(4) + 1)).asSymbol, (inc == this.dataMapView.dmParticleID).if({ 1 }, { 0 }))
			}
		})

	}

	resetOSCAddr { |aNetAddr|
		learn.isLearning.if({ learn.stop });
		this.disconnect;
		learn.disconnect;
		learn.addr_(aNetAddr);
		this.connect;
		button.value_(2);
		this.setAddrText
	}

	saveSettings {
		^[learn.addr, buttonState]
	}

	loadSettings { |settings, loadOSC = false|
		var settingsAddr;
		#settingsAddr, buttonState = settings;
		loadOSC.if({
			learn.isLearning.if({ learn.stop });
			settingsAddr !? { this.learn.addr_(settingsAddr) };
			this.connect;
			this.updateButtonStates;
		})
	}

	updateButtonStates {
		button.value_(buttonState);
		this.setAddrText
	}

	cleanup {
		this.disconnect;
		learn.disconnect;
		xyFuncs.do{ |func| this.dataMapView.removeDrawPointsFunc(func) };
		removeTasks.do(_.stop)
	}

}