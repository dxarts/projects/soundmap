DataMap {
	var <>data;
	var <dim;
	var <view, <kdTree;

	*new { |data|
		^super.newCopyArgs(data).init
	}

	init {
		dim = data[0].asArray.size;
		kdTree = KDTree.new(this.checkRank(data).collect{ |item, i| item.asArray ++ [i]}, lastIsLabel: true);
	}

	checkRank { |data|
		data = data.collect(_.asArray);
		^(data.rank > 2).if({ data.collect{ |dat| dat.flatten } }, { data })
	}

	nearestKD { |dataPoint| ^kdTree.nearest(dataPoint.asArray) }
	nearestKDIndex { |dataPoint| ^this.nearestKD(dataPoint)[0].label }
	nearestKDDist { |dataPoint| ^this.nearestKD(dataPoint)[1] }

	nearestKDN { |dataPoint, numRes|
		var nearN, radius;
		nearN = [];
		radius = 0.1;
		while({ nearN.size != numRes.asInteger }, {
			nearN = kdTree.radiusSearch(dataPoint.asArray, radius);
			radius = (nearN.size > numRes).if( {radius * 0.9 }, { radius * 1.1 });
		});
		^nearN
	}

	nearestN { |point, axes|
		axes = axes ?? { (0..dim-1) };
		point = point.asArray;
		axes = axes.asArray;
		// if user has give full point data, reduce to the given axes
		(point.size != axes.size).if({ point = point[axes] });
		^data.collect{ |thisPoint|
			this.dist(thisPoint.asArray[axes], point)
		}
	}

	nearestDistance { |point, axes|
		^this.nearestN(point, axes).minItem
	}

	nearestIndex { |point, axes|
		^this.nearestN(point, axes).minIndex
	}

	orderedIndices { |point, axes|
		^this.nearestN(point, axes).order
	}

	dist { |point1, point2|
		var dist;
		point1 = point1.asArray;
		point2 = point2.asArray;
		(point1.size == point2.size).not.if({
			"The two points do not have the same number of dimensions".warn
		});

		point1.do{ |point, i|
			(point.isKindOf(Array) or: point2[i].isKindOf(Array)).if({
				dist = dist.add(DTW.calc(point.asArray, point2[i].asArray))
			}, {
				dist = dist.add(point - point2[i])
			})
		};
		^dist.squared.sum.sqrt

	}

	size { ^data.size }
	labels { ^(0..this.size-1) }


}

