DataMapView : View {
	var <points, <colors, <dataMap;
	var <funcNames;
	var <window, <view, <userView;
	var <>mouseDownFunc, <>mouseMoveFunc, <>drawPointsFunc, <>cleanupFunc;
	var pen, <cen;
	var <minDim;
	var <pointColors, <pixelSize = 15, <frameRate = 5;
	var <pixelPoints, <playingParticles, <playingColors;
	var <>dmParticles, <>midi, <>touchOSC, <>loadOSC, tempDownFuncs, tempMoveFuncs;
	var <grid, <>showGrid = false;
	var <pTxt, playFunc, <>dist;
	var <offCol, <onCol, pixelInSet, <>center, <>centerConnected = false;
	var <numParticles, <particleSettings;
	var <>mouseFuncDown, <>mouseFuncMove, <>dmParticleID, <masterReset, <particlesView, <enableButtons, <>dmSettings;
	var <currentColor, <drawRefresh;


	*new { |points, colors, playFunc, numParticles, parent, bounds = (Window.screenBounds), loadOSC, initializeOSC = false, showParticleSettings = true|
		^super.new(parent, bounds).init(points, colors, playFunc, numParticles, loadOSC, initializeOSC, showParticleSettings)
	}

	init { |argPoints, argColors, argPlay, argParts, argOSC, arginitOSC, argspc|
		loadOSC = argOSC ?? { false };
		points = argPoints;
		colors = argColors ?? { points.size.collect{ Color.black } };
		playFunc = argPlay;
		pointColors = colors;
		points = points.collect(_.asPoint);
		cen = this.bounds.center;
		pixelSize = ((this.bounds.width + this.bounds.height) * 0.007).asInteger.clip(6, 15);
		pixelInSet = ((this.bounds.width + this.bounds.height) * 0.02).asInteger.clip(4, 20);
		minDim = Point(this.bounds.width - pixelInSet, this.bounds.height - pixelInSet);
		pixelPoints = points.collect{ |point| this.translateToPixels(point) };
		dataMap = DataMap.new(points);
		funcNames = ['mouseDownFunc', 'mouseMoveFunc', 'drawPointsFunc'];
		this.removeAllFuncs;
		userView = UserView(this, this.bounds.origin_(0@0))
		.resize_(5)
		.frameRate_(frameRate)
		.drawFunc_(this.drawFunc);

		dmParticleID = 0;

		// resize with parent
		this.resize_(5);
		this.onResize_({
			pixelSize = ((this.bounds.width + this.bounds.height) * 0.007).asInteger.clip(6, 15);
			pixelInSet = ((this.bounds.width + this.bounds.height) * 0.02).asInteger.clip(4, 20);
			minDim = Point(this.bounds.width - pixelInSet, this.bounds.height - pixelInSet);
			pixelPoints = points.collect{ |point| this.translateToPixels(point) };
			grid.bounds_(this.bounds);
			this.refresh;
		});

		playingParticles = [];
		userView.mouseDownAction_({
			|v,x,y, modifiers, buttonNumber, clickCount|
			mouseDownFunc.value(this.translateFromPixels(Point(x, y)))
		});

		userView.mouseMoveAction_({
			|v,x,y, modifiers, buttonNumber, clickCount|
			mouseMoveFunc.value(this.translateFromPixels(Point(x, y)))
		});
		offCol = Color.black;
		onCol = Color.blue;

		grid = DrawGrid(this.bounds, ControlSpec(-1.0, 1.0).grid, ControlSpec(-1.0, 1.0).grid);
		center = Cartesian.new;



		argParts.do{ |inc| this.addParticleSettings(playFunc.asArray.wrapAt(inc), inc) };

		currentColor[dmParticleID] = Color.blue;

		particleSettings = Window.new("Particle Settings", Window.screenBounds);

		particlesView = DMParticlesView(this, dmParticles[dmParticleID], dmSettings[dmParticleID]);

		touchOSC = DMTouchOSC.new(this, dmParticles[dmParticleID]);

		arginitOSC.if({ touchOSC.button.valueAction_(1) });

		masterReset = Button.new(particleSettings).states_([
			["Reset All Particles", Color.black, Color.white]
		])
		.action_({ |butt|
			dmParticles.do(_.resetParticles)
		});

		enableButtons = argParts.collect{ |inc|
			Button(particleSettings).states_([
				["Particle Settings" + (inc + 1), offCol, Color.white],
				["Particle Settings" + (inc + 1), onCol, Color.white]
			])
			.action_({ |butt|
				(butt.value == 1).if({
					var indices = (0..argParts-1);
					dmParticleID = inc;
					this.particlesView.closeOpenFuncs;
					this.particlesView.dmParticles_(this.dmParticles[dmParticleID]).dmSettings_(this.dmSettings[dmParticleID]);
					this.touchOSC.dmParticles_(this.dmParticles[dmParticleID]);
					this.particlesView.updateGUIElements;
					currentColor = currentColor.collect{ offCol };
					currentColor[dmParticleID] = onCol;
					indices.removeAt(inc);
					enableButtons[indices].do{ |button| button.value_(0) };
					this.touchOSC.updateTouchOSC
				})
			})
		};

		particleSettings.layout_(
			VLayout(
				HLayout(
					this.layItOut,
					touchOSC.makeWindow
				),
				masterReset,
				HLayout(*enableButtons[0..3]),
				HLayout(*enableButtons[4..]),
				particlesView.layOut

			)
		);

		argspc.if({ particleSettings.front });
		particlesView.guiElements['nontoggle']['/settings/click'].valueAction_(1);

		enableButtons[dmParticleID].valueAction_(1);

		this.addFuncs;

		drawRefresh = Task({
			loop{
				{ this.refresh }.defer;
				this.frameRate.reciprocal.wait
			}
		}).play;

		this.onClose_({
			this.cleanup
		})


	}

	layItOut {


		^HLayout(
			[StaticText().string_("Load Settings")
				.mouseDownAction_({ |txt|
					(
						Dialog.openPanel({ |path|
							this.loadSettings(path) },
						path: Platform.userAppSupportDir);
					)

				})
				.stringColor_(offCol), align: \topLeft],
			[StaticText().string_("Save Settings")
				.mouseDownAction_({ |txt|
					(
						Dialog.savePanel({ |path|
							this.saveSettings(path) },
						path: Platform.userAppSupportDir);
					)

				})
				.stringColor_(offCol), align: \topLeft],
			[StaticText().string_("TouchOSC:")
				.stringColor_(offCol), align: \topLeft]
		)
	}

	addParticleSettings { |func, id|
		var dmParticle;
		dmParticle = DMParticles.new(this.dataMap, func, id);
		dmParticles = dmParticles.add(dmParticle);
		currentColor = currentColor.add(offCol);
		dmSettings = dmSettings.add(DMParticlesViewSettings.new);
		dmParticle.runFunc = dmParticle.runFunc.addFunc {
			dmParticle.uniqueTrees !? { this.playingColors_(dmParticle.uniqueTrees.flop[0].collect(_.label)) };
			// { this.refresh }.defer
		};

		// dmParticle.rhythmFunc = dmParticle.rhythmFunc.addFunc {
		// 	{ this.refresh }.defer
		// };

		dmParticle.dmRhythm.playFunc = dmParticle.dmRhythm.playFunc.addFunc{ |kdtrees, id, time| this.playingColors_(kdtrees.flop[0].collect(_.label), time) };

		dmParticle.dist = 30.linlin(0, this.bounds.width, 0, 1) * 1.5;

		dmParticle.dmRhythm.stopFunc = dmParticle.dmRhythm.stopFunc.addFunc({
			(this.dmParticleID == id).if({
				{ particlesView.guiElements['toggle']['/rhythm/play'].valueAction_(0) }.defer
			})
		});

		this.addDrawPointsFunc({
			(dmParticle.playingState < 3).if({
				Pen.fillColor_(currentColor[id]);
				Pen.fillOval(Size(this.pixelSize, this.pixelSize).asRect
					.center_(this.translateToPixels(dmParticle.particles.centerLocation)));
				particlesView.centerlocationX.value_(dmParticle.particles.centerLocation.x.round(0.01));
				particlesView.centerlocationY.value_(dmParticle.particles.centerLocation.y.round(0.01))
			})
		});

		this.addDrawPointsFunc({
			var tempPoint;
			(dmParticle.playingState == 2).if({
				Pen.fillColor_(currentColor[id]);
				dmParticle.dmRhythm.tempAddPoint !? {
					tempPoint = dmParticle.dmRhythm.tempAddPoint.isKindOf(Array).if({ dmParticle.dmRhythm.tempAddPoint }, { [dmParticle.dmRhythm.tempAddPoint] });
					tempPoint.do{ |point|
						Pen.addArc( this.translateToPixels(dmParticle.dmRhythm.getLoc(point)), this.pixelSize + 10 / 2, 0, 2pi )
					}
				};
				Pen.fill;
				Pen.fillColor_(offCol);
				dmParticle.dmRhythm.tempRemovePoint !? {
					Pen.addArc( this.translateToPixels(dmParticle.dmRhythm.getLoc(dmParticle.dmRhythm.tempRemovePoint)), this.pixelSize + 5 / 2, 0, 2pi )
				};
				Pen.fill;
				Pen.fillColor_(currentColor[id]);
				dmParticle.dmRhythm.points.size.do{ |point|
					dmParticle.dmRhythm.tempRemovePoint.notNil.if({
						(dmParticle.dmRhythm.tempRemovePoint != dmParticle.dmRhythm.points[point]).if({
							Pen.addArc( this.translateToPixels(dmParticle.getPointLoc(point)), this.pixelSize + 5 / 2, 0, 2pi )
						})
					}, {
						Pen.addArc( this.translateToPixels(dmParticle.dmRhythm.getPointLoc(point)), this.pixelSize + 5 / 2, 0, 2pi )
					})
				};
				Pen.fill
			})
		})
	}

	addFuncs {

		this.addMouseDownFunc({ |cart|
			(this.dmParticles[dmParticleID].playingState < 2).if({
				this.dmParticles[dmParticleID].addParticle(cart)
			})
		});

		this.addMouseMoveFunc({ |cart|
			(this.dmParticles[dmParticleID].playingState < 2).if({
				this.dmParticles[dmParticleID].addParticle(cart)
			})
		});

		this.addMouseDownFunc({ |point|
			(this.dmParticles[dmParticleID].playingState == 3).if({
				this.dmParticles[dmParticleID].playFunc.(this.dmParticles[dmParticleID].clickPoints(point, this.dmParticles[dmParticleID].dist), dmParticleID);
				this.dmParticles[dmParticleID].uniqueTrees.notEmpty.if({ this.playingColors_(this.dmParticles[dmParticleID].uniqueTrees[0][0].label, 0.1) });
				{ this.refresh }.defer
			})
		});

		this.addMouseMoveFunc({ |point|
			(this.dmParticles[dmParticleID].playingState == 3).if({
				this.dmParticles[dmParticleID].playFunc.(this.dmParticles[dmParticleID].clickPoints(point, this.dmParticles[dmParticleID].dist, false), dmParticleID);
				this.dmParticles[dmParticleID].uniqueTrees.notEmpty.if({ this.playingColors_(this.dmParticles[dmParticleID].uniqueTrees[0][0].label, 0.5) });
				{ this.refresh }.defer
			})
		});

	}

	resetOSCAddr { |netAddr|
		touchOSC.resetOSCAddr(netAddr)
	}

	drawFunc {
		^{
			var pixels, partPixels, particles, removal;
			showGrid.if({ grid.draw });
			drawPointsFunc.value;
			dmParticles !? {
				particles = dmParticles.collect{ |dmParticle|
					dmParticle.particles.particles
				}.flatten ;
				partPixels = particles.collect{ |particle| this.translateToPixels(particle.location.asPoint)
				}
			};
			pixels = pixelPoints ++ partPixels;
			playingParticles.do{ |arr| (arr[1] <= 0.0).if({ removal = removal.add(arr) }, { arr[1] = arr[1] - 0.1 }) };
			removal.do{ |item| playingParticles.remove(item) };
			this.playingColors_;
			pointColors = playingColors ++ particles.collect{ |particle| Color.gray(0.0, particle.lifeSpan.clip(0.0, 1.0)) };

			pixels.do{ |pixel, i|
				// draw point
				Pen.fillColor_(pointColors.wrapAt(i));
				Pen.fillOval(Size(pixelSize, pixelSize).asRect.center_(pixel))
			}
		}
	}

	loadSettings { |path|
		var tempDMParticles, tempDMSettings, tempMIDI, tempOSC, tempConnected;
		#dmParticleID, tempDMParticles, tempDMSettings, tempMIDI, tempOSC = Object.readArchive(path);

		this.dmSettings_(tempDMSettings);
		this.particlesView.dmSettings_(this.dmSettings[dmParticleID]);
		this.dmParticles.do{ |dmParticle, inc| dmParticle.loadSettings(tempDMParticles[inc]) };
		this.particlesView.updateGUIElements;
		tempMIDI !? {
			MIDIIn.connectAll;
			this.midi_(DMLearn.new(this, 5, MIDILearn));
			this.midi.loadSettings(tempMIDI);
			tempConnected = this.midi.centerConnected;
			(this.centerConnected.not and: tempConnected).if({ this.centerConnected_(true) });
			this.midi.connect;
			this.midi.makeWindow.window.front;
			this.midi.updateButtonStates
		};
		tempOSC !? {
			tempOSC.isKindOf(DMLearn).if({
				this.osc_(DMLearn.new(this, 5, OSCLearn));
				this.osc.loadSettings(tempOSC);
				tempConnected = this.osc.centerConnected;
				(this.centerConnected.not and: tempConnected).if({ this.centerConnected_(true) });
			}, {
				this.touchOSC.loadSettings(tempOSC, this.loadOSC);

			});
		}
	}

	translateSettings { |path, newPath|
		var settings = path.load, size, tempDMParticles, tempSettings;

		newPath ?? { newPath = path };
		dmParticleID = settings[0];
		size = settings[1].size;
		tempDMParticles = Array.fill(size, { DMParticles.new(this, {}) });
		tempSettings = Array.fill(size, { DMParticlesViewSettings.new });
		settings[1].do{ |setting, inc|
			tempDMParticles[inc].playingState_ (setting[3]);
			tempDMParticles[inc].dmRhythm.points_(setting[5]);
			tempDMParticles[inc].dmRhythm.inc_(setting[6]);
			tempDMParticles[inc].particles.centerLocation_(setting[7]);
			tempSettings[inc].guiElements['nontoggle']['addParticleX'] = setting[0]['addParticleX'];
			tempSettings[inc].guiElements['nontoggle']['addParticleY'] = setting[0]['addParticleY'];
			tempSettings[inc].guiElements['toggle']['/rhythm/addpoint'] =  setting[1]['/rhythm/addpoint'];
			tempSettings[inc].guiElements['toggle']['/rhythm/addpoints'] =  setting[1]['/rhythm/addpoints'];
			tempSettings[inc].guiElements['toggle']['/rhythm/removepoint'] =  setting[1]['/rhythm/removePoint'];
			tempDMParticles[inc].bounce = setting[1]['/settings/bounce'].asBoolean;
			tempDMParticles[inc].dmRhythm.heap = setting[1]['/rhythm/heap'].asBoolean;
			tempDMParticles[inc].startVel = setting[2]['/settings/velocity'];
			tempDMParticles[inc].maxParticles = setting[2]['/settings/maxparticles'];
			tempDMParticles[inc].particles.maxspeed = setting[2]['/flock/maxspeed'];
			tempDMParticles[inc].particles.centerForce = setting[2]['/flock/centerforce'];
			tempDMParticles[inc].particles.lifeRate = setting[2]['/settings/liferate'];
			tempDMParticles[inc].particles.maxforce = setting[2]['/flock/maxforce'];
			tempDMParticles[inc].particles.attraction = setting[2]['/flock/attraction'];
			tempDMParticles[inc].particles.repulsion = setting[2]['/flock/repulsion'];
			tempDMParticles[inc].bounceVel = setting[2]['/settings/bouncevel'];
			tempDMParticles[inc].dist = (setting[1]['/settings/dist'] == 1).if({ nil }, { 30.linlin(0, this.bounds.width, 0, 1) * 1.5 })

		};

		[dmParticleID, tempDMParticles.collect(_.saveSettings), tempSettings, settings[2], settings[3]].writeArchive(newPath)

	}

	saveSettings { |path|
		[this.dmParticleID, this.dmParticles.collect(_.saveSettings), this.dmSettings, this.midi !? { this.midi.saveSettings }, this.touchOSC !? { this.touchOSC.saveSettings }].writeArchive(path)
	}

	frameRate_ { |hz|
		frameRate = hz;
		userView.frameRate_(hz);
		userView.refresh;
	}

	pixelSize_ { |newPixelSize| pixelSize = newPixelSize }

	translateToPixels { |point| ^Point(point.x.linlin(-1, 1, pixelInSet, minDim.x, nil), point.y.linlin(-1, 1, minDim.y, pixelInSet, nil)) }

	translateFromPixels { |point| ^Point(point.x.linlin(pixelInSet, minDim.x, -1, 1,  nil), point.y.linlin(pixelInSet, minDim.y, 1, -1, nil)) }

	removeAllFuncs {
		funcNames.do{ |funcName|
			this.perform(funcName.asSetter, FunctionList.new)
		}
	}

	refresh { userView.refresh }

	playingColors_ { |indices, time = 0.5|
		playingColors = this.colors.copy;
		indices.asArray.do{ |i| playingParticles = playingParticles.add([i, time]).asSet.asArray };
		playingParticles.do{ |arr| playingColors[arr[0]] = Color.gray };
	}

	addMouseDownFunc { |func| this.addFuncTo('mouseDownFunc', func) }
	addMouseMoveFunc { |func| this.addFuncTo('mouseMoveFunc', func) }
	overrideMouseDownFunc { |func| tempDownFuncs = mouseDownFunc; mouseDownFunc = func }
	overrideMouseMoveFunc { |func| tempMoveFuncs = mouseMoveFunc; mouseMoveFunc = func }
	resumeMouseDownFunc { mouseDownFunc = tempDownFuncs }
	resumeMouseMoveFunc { mouseMoveFunc = tempMoveFuncs }
	removeMouseDownFunc { |func| this.removeFuncFrom('mouseDownFunc', func) }
	removeMouseMoveFunc { |func| this.removeFuncFrom('mouseMoveFunc', func) }
	addDrawPointsFunc { |func| this.addFuncTo('drawPointsFunc', func) }
	removeDrawPointsFunc { |func| this.removeFuncFrom('drawPointsFunc', func) }



	cleanup {
		particleSettings !? { particleSettings.close };
		cleanupFunc.value;
		dmParticles.do(_.cleanup);
		midi !? { midi.cleanup };
		touchOSC !? { touchOSC.cleanup };
		drawRefresh.stop;
		this.close;
	}

	free { this.cleanup }

}