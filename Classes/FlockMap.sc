// Flock dependecy

FlockMap {
	var <points, <files, <flock;
	var <duration, <flockEnvs, <nearestNeighbor;
	var numPoints, flockSpeed;

	*new { |points, files, flock|
		^super.newCopyArgs(points, files, flock).init
	}

	init {

		nearestNeighbor = NearestNeighbor(points);
		numPoints = this.flock.boids.size + points.size;
	}

	generateFlockEnvs { |speed, cForce, cLocation|

		flockSpeed = speed;

		duration = flockSpeed.duration;

		flockEnvs = flock.flockToEnvs(flockSpeed, cForce, cLocation, 1).flockEnvs;

	}

	render { |outPath, thresholdEnv, repetitionTimer = 5.0|
		var buffers, score, synths, now, location, buffer, nearestIndex, distance, lastIndices;

		Server.default.newAllocators;

		now = 0.0;
		score = CtkScore.new;
		buffers = files.collect{ |file|
			CtkBuffer(file.fullPath).addTo(score)
		};
		synths = CtkProtoNotes(
			SynthDef('playBuf', {
				arg buffer, outbus, dur;
				var src, env;
				env = EnvGen.kr(Env([0, 1, 0], [0.01, dur - 0.02, 0.01], 'sin'));
				src = PlayBuf.ar(buffers[0].numChannels, buffer, BufRateScale.kr(buffer));
				Out.ar(outbus, src * env)
			})
		);

		lastIndices = buffers.size.collect({0.0});
		while({ now <= duration }, {
			flockEnvs.do{ |flockEnv, i|
				location = Cartesian(flockEnv.x[now], flockEnv.y[now], flockEnv.z[now]);
				#nearestIndex, distance = nearestNeighbor.nearestIndex(location, [0, 1]);
				((distance <= thresholdEnv[now]) and: (lastIndices[nearestIndex] <= 0.0)).if({
					buffer = buffers[nearestIndex];
					synths['playBuf'].note(now, buffer.duration)
					.buffer_(buffer)
					.dur_(buffer.duration)
					.outbus_(0)
					.addTo(score);
					lastIndices[nearestIndex] = repetitionTimer.value;
				})
			};
			now = now + flockSpeed[now];
			lastIndices.collectInPlace({ |time| time - flockSpeed[now] });
		});

		score.saveToFile("/Users/dan/Desktop/flockMap.sc");
		score.write(
			path: outPath,
			headerFormat: 'WAV',
			sampleFormat: 'float',
			sampleRate: 48000,
			options: ServerOptions.new.numOutputBusChannels_(16)
		)

	}

	play { |thresholdEnv, repetitionTimer = 5.0, server|
		var window, pointView, now = 0, thesePoints, task, buffers, synths;
		var colors, lastIndices, buffer, distance;

		window = Window.new("FlockMap", Window.screenBounds);

		server = server ?? { Server.default };

		thesePoints = points.collect(_.asCartesian);
		pointView = PointView.new(window, window.bounds).points_(thesePoints).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);
		{
			buffers = files.collect{ |file|
				CtkBuffer(file).load
			};

			server.sync;
			synths = CtkProtoNotes(
				SynthDef('playBuf', {
					arg buffer, outbus, dur;
					var src, env;
					env = EnvGen.kr(Env([0, 1, 0], [0.01, dur - 0.02, 0.01], 'sin'));
					src = PlayBuf.ar(buffers[0].numChannels, buffer, BufRateScale.kr(buffer));
					Out.ar(outbus, src * env)
				})
			);

			window.front;

			lastIndices = buffers.size.collect({0.0});

			task = Task({
				var nearestIndex, distances;
				while({ now <= duration }, {
					var nowPoints;
					nowPoints = flockEnvs.collect{ |flockEnv| [flockEnv.x[now], flockEnv.y[now], flockEnv.z[now]].asCartesian };
					colors = Color.black.dup(numPoints);
					nowPoints.do{ |point, i|
						distances = thesePoints.collect{ |thisPoint| thisPoint.dist(point) };
						nearestIndex = distances.minIndex;
						distance = distances[nearestIndex];
						(distance <= thresholdEnv[now]).if({
							colors[nearestIndex] = Color.red;
							(lastIndices[nearestIndex] <= 0.0).if({
								buffer = buffers[nearestIndex];
								synths['playBuf'].note(0.0, buffer.duration)
								.buffer_(buffer)
								.dur_(buffer.duration)
								.outbus_(0)
								.play;
								lastIndices[nearestIndex] = repetitionTimer;
							})
						})
					};
					{ pointView.points_(thesePoints ++ nowPoints).pointColors_(colors) }.defer;
					now = now + flockSpeed[now];
					lastIndices.collectInPlace({ |time| time - flockSpeed[now] });
					flockSpeed[now].wait
				})
			}).play;

			window.onClose_({
				task.stop;
				buffers.do(_.free);
			})

		}.forkIfNeeded

	}

	plot {
		var window, pointView, now = 0, thesePoints, task, colors, numPoints;
		window = Window.new("FlockMap", Window.screenBounds);

		thesePoints = points.linlin(0.0, 1.0, -1.0, 1.0).collect(_.asCartesian);
		pointView = PointView.new(window, window.bounds).points_(thesePoints).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);

		colors = Color.black.dup(numPoints);
		window.front;

		task = Task({
			var location, nearestIndex;
			while({ now <= duration }, {
				var nowPoints;
				nowPoints = flockEnvs.collect{ |flockEnv| [flockEnv.x[now], flockEnv.y[now], flockEnv.z[now]].asCartesian };
				colors = Color.black.dup(numPoints);
				flockEnvs.do{ |flockEnv, i|
					location = Cartesian(flockEnv.x[now], flockEnv.y[now], flockEnv.z[now]);
					nearestIndex = nearestNeighbor.nearestIndex(location);
					colors[nearestIndex] = Color.red
				};
				{ pointView.points_(thesePoints ++ nowPoints).pointColors_(colors) }.defer;
				now = now + flockSpeed[now];
				flockSpeed[now].wait
			})
		}).play;

		window.onClose_({
			task.stop
		})

	}


}