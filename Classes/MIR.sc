MIR {
	var <soundFilePath;
	var <data, <scMIR;

	*new { |soundFilePath|
		^super.newCopyArgs(soundFilePath)
	}

	scmir { |analysisType, args|
		scMIR = SCMIRAudioFile.new(soundFilePath, [[analysisType] ++ args]);
		scMIR.extractFeatures(false);
		data = scMIR.featuredata
	}

	mfcc { |numCoeffs = 13, args|
		^this.scmir(MFCC, args ? [numCoeffs])
	}

	chromogram { |numOctaveDivisions = 12, tuningBase = ('C0'.namecps), numOctaves = 8, integrationFlag = 0, integrationCoeff = 0.9, octaveRatio = 2, perFrameNormalize = 0, args|
		^this.scmir(Chromagram, args ? [numOctaveDivisions, tuningBase, numOctaves, integrationFlag, integrationCoeff, octaveRatio, perFrameNormalize])
	}

	keyClarity { |keyDecay = 2.0, chromaleak = 0.5, args|
		^this.scmir(KeyClarity, args ? [keyDecay, chromaleak])
	}

	keyTrack { |keyDecay = 2.0, chromaleak = 0.5, args|
		^this.scmir(KeyTrack, args ? [keyDecay, chromaleak])
	}

	keyMode { |keyDecay = 2.0, chromaleak = 0.5, args|
		^this.scmir(KeyMode, args ? [keyDecay, chromaleak])
	}

	spectralEntropy { |args|
		^this.scmir(SpectralEntropy)
	}

	tartini { |args|
		^this.scmir(Tartini)
	}

	beatTrack { |args|
		^this.scmir(BeatTrack)
	}

	loudness { |smask = 0.25, tmask = 1, args|
		^this.scmir(Loudness, args ? [smask, tmask])
	}

	sensoryDissonance { |maxPeaks = 100, peakThreshold = 0.1, norm, clamp = 1.0, args|
		^this.scmir(SensoryDissonance, args ? [maxPeaks, peakThreshold, norm, clamp])
	}

	specCentroid { |args|
		^this.scmir(SpecCentroid)
	}

	specPcile { |fraction = 0.5, args|
		^this.scmir(SpecPcile, args ? [fraction])
	}

	specFlatness { |args|
		^this.scmir(SpecFlatness)
	}

	fftCrest { |freqLo = 0, freqHi = 50000, args|
		^this.scmir(FFTCrest, args ? [freqLo, freqHi])
	}

	fftSpread {
		^this.scmir(FFTSpread)
	}

	fftSlope {
		^this.scmir(FFTSlope)
	}

	onsets { |odftype = 'rcomplex', args|
		^this.scmir(Onsets, args ? [odftype])
	}
}