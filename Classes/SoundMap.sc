SoundMap {
	var <>modelPath, <>python;
	var <>data, <>rawData, <>descriptors, <>files, <>relativeFiles, <>dataType;
	var <>tsne_2, <>tsne_3, <>tsne_3f, <>tsne_3u, <analysis, <>colors_2, <>colors_3, <>colors_3f, <>colors_3u, <>rawcolors_2, <>rawcolors_3, <>rawcolors_3f, <>rawcolors_3u;
	var <>normalizations, <normTypes, <offset, <dataMap, <>axes;
	var <window, <dataMapView;
	var <>nearestFunc, <>onClose;
	var <>buffers, <note, <>pathToPython;
	classvar <>soundMapPath;

	*initClass {
		soundMapPath = File.realpath(SoundMap.filenameSymbol.asString.dirname.dirname)
	}

	*new { |pathToPython|
		^super.new.init(pathToPython)
	}

	*newModel { |mapPath, type = 'librosa', descriptors = (['fft']), fftSize = 8192, hopSize = 0.25, sampleRate = 48000, interpl = 100, verbose = false, pathToPython|
		var inst;
		inst = SoundMap.new(pathToPython);
		inst.runModel(mapPath, type, descriptors, fftSize, hopSize, sampleRate, interpl, verbose);
		inst.modelPath_(mapPath +/+ "Data" +/+ type ++ ".json");
		inst.read2(inst.modelPath)
		^inst
	}

	*gui {
		var modelPath, loadData = false, dataType = 'ml', loadFiles = true, loadDescriptors = false, loadTSNE = false, loadColors = false, verbose = false;

	}

	*read { |modelPath, loadData = false, dataType = 'ml', loadFiles = true, loadDescriptors = false, loadTSNE = false, loadColors = false, verbose = false, pathToPython, pathMod = "/../samples/"|
		^(modelPath.extension == "sav").if({
			SoundMap.new(pathToPython).read(modelPath, loadData, dataType, loadFiles, loadDescriptors, loadTSNE, loadColors, verbose, pathMod)
		}, {
			SoundMap.new(pathToPython).read2(modelPath, dataType, pathMod)
		})
	}

	init { |apython|
		this.python_(Python.new(apython))
	}

	read { |newModelPath, loadData = false, dataType = 'ml', loadFiles = true, loadDescriptors = false, loadTSNE = false, loadColors = false, verbose = false, pathMod = "/../samples/"|
		var cmd, paths, colorsTypes = [];
		newModelPath !? { this.modelPath = newModelPath };
		this.dataType = dataType;
		paths = 10.collect{ Platform.defaultTempDir ++ UniqueID.next ++ ".csv" };
		paths[0] = paths[0].replace("csv", "npy");
		cmd = "cd" + SoundMap.soundMapPath.escapeChar($ ) ++ ";" + this.python.python + "scripts/ReadData.py";
		cmd = cmd + this.modelPath.escapeChar($ );
		paths.do{ |path| cmd = cmd + path.escapeChar($ ) };
		cmd = cmd + "-d" + dataType;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		loadData.if({ this.data = this.python.fromNumpy(paths[0], verbose).asFloat });
		loadFiles.if({
			{ this.relativeFiles = CSVFileReader.read(paths[1]).flatten }.try{ "No files were loaded".warn }
		});
		loadDescriptors.if({
			{ this.descriptors = CSVFileReader.read(paths[2]).flatten }.try{ "No Descriptors were loaded".warn }
		});
		loadTSNE.if({
			{ this.tsne_2 = CSVFileReader.read(paths[3]).asFloat.collect(_.asPoint);
				colorsTypes = colorsTypes.add('2')
			}.try{ "No TSNE_2 data were loaded".warn };
			{ this.tsne_3 = CSVFileReader.read(paths[4]).asFloat.collect(_.asCartesian);
				colorsTypes = colorsTypes.add('3')
			}.try{ "No TSNE_3 data were loaded".warn };
			{ this.tsne_3f = CSVFileReader.read(paths[5]).asFloat.collect(_.asCartesian);
				colorsTypes = colorsTypes.add('3f')
			}.try{ "No TSNE_3f data were loaded".warn };
			{ this.tsne_3u = CSVFileReader.read(paths[6]).asFloat.collect(_.asCartesian);
				colorsTypes = colorsTypes.add('3u')
			}.try{ "No TSNE_3u data were loaded".warn }
		});
		loadColors.if({
			{
				colorsTypes.do{ |type, i|
					this.translateColors(type, CSVFileReader.read(paths[7 + i]).asInteger.flatten + 1)
				}
			}.try{ "No color data were loaded".warn }
		});
		paths.do{ |path| File.delete(path) };
		this.files = this.relativeFiles !? { this.relativeFiles.collect{ |relPath| File.realpath(modelPath.dirname ++ pathMod ++ relPath) } };
	}


	read2 { |jsonPath, dataType = 'ml', pathMod = "/../samples/"|
		var randColors, json, colorTypes = ['2', '3', '3f'];
		jsonPath !? { this.modelPath = jsonPath.replace("json", "sav") };
		this.dataType = dataType;

		json = jsonPath.parseJSONFile;
		this.data = (this.dataType == 'ml').if({
			json["audioml_data"].asFloat
		}, {
			json["analysis_data"].asFloat
		});
		this.rawData = json["raw_data"] !? {json["raw_data"].asFloat};
		this.relativeFiles = json["relative_files"];
		this.axes = json["axes"] !? {json["axes"].asInteger};
		this.descriptors = json["descriptors"];
		this.tsne_2 = json["tsne_2"].asFloat;
		this.tsne_3 = json["tsne_3"].asFloat;
		this.tsne_3f = json["tsne_3f"].asFloat;
		{ this.tsne_3u = json["tsne_3u"].asFloat; colorTypes = colorTypes.add('3u') }.try{ "No TSNE_3u data were loaded".warn };
		colorTypes.do{ |type, i| this.translateColors(type, json["colors_" ++ type].asInteger.flatten + 1) };
		this.files = this.relativeFiles !? { this.relativeFiles.collect{ |relPath| File.realpath(modelPath.dirname ++ pathMod ++ relPath) } }

	}

	translateColors { |type, rawColors|
		var randColors;
		this.perform(('rawcolors_' ++ type).asSymbol.asSetter, rawColors);
		randColors = (this.perform(('rawcolors_' ++ type).asSymbol).maxItem + 1).collect{ Color.rand };
		this.perform(('colors_' ++ type).asSymbol.asSetter, this.perform(('rawcolors_' ++ type).asSymbol).collect{ |color| randColors[color] })
	}

	runModel { |mapPath, type = 'librosa', descriptors = (['fft']), fftSize = 8192, hopSize = 0.25, sampleRate = 48000, interpl = 100, verbose = false|
		var cmd;
		cmd = "cd" + soundMapPath ++ ";" + this.python.python + "scripts/RunModel.py";
		cmd = cmd + mapPath.escapeChar($ );
		cmd = cmd + "-a" + type;
		cmd = cmd + "-d"; descriptors.do{ |descriptor| cmd = cmd + descriptor };
		cmd = cmd + "-f" + fftSize;
		cmd = cmd + "-p" + hopSize;
		cmd = cmd + "-s" + sampleRate;
		cmd = cmd + "-i" + interpl;
		verbose.if({ cmd.postln });
		cmd.systemCmd;

	}

	kMeansColors { |data, savePath, verbose = true|
		var cmd, tmpColorData;
		tmpColorData = Platform.defaultTempDir +/+ "colordata.txt";
		python.writeArray2D(data, tmpColorData);
		cmd = "cd" + soundMapPath ++ ";" + (this.python.python ?? { Python.pathToPython }) + "scripts/Colors.py";
		cmd = cmd + tmpColorData.escapeChar($ ) + savePath.escapeChar($ );
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		File.delete(tmpColorData);
		^python.fromNumpy(savePath).asInteger.flatten
	}

	pythonPredict { |soundPath, type = "Prediction", verbose = true|
		var cmd;
		cmd = "cd" + soundMapPath ++ ";" + (this.python.python ?? { Python.pathToPython }) + "scripts/" ++ type ++ ".py";
		cmd = cmd + modelPath.escapeChar($ ) + soundPath.escapeChar($ );
		verbose.if({ cmd.postln });
		^cmd.unixCmdGetStdOut.replace("\n");
	}

	predict { |soundPath, verbose = true|
		^this.pythonPredict(soundPath, verbose: verbose)
	}

	dtw { |soundPath, verbose = true|
		^this.pythonPredict(soundPath, "DTW", verbose)
	}

	modelAnalysisData { |soundPath, verbose = false|
		var cmd, path;
		path = Platform.defaultTempDir ++ UniqueID.next ++ ".npy";
		cmd = "cd" + soundMapPath.escapeChar($ ) ++ ";" + (this.python.python ?? { Python.pathToPython }) + "scripts/ModelAnalysis.py";
		cmd = cmd + soundPath.escapeChar($ );
		cmd = cmd + modelPath.escapeChar($ );
		cmd = cmd + path.escapeChar($ );
		cmd = cmd + "-d" + dataType;
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		^python.fromNumpy(path, verbose)
	}

	analysisData { |soundPath, type, descriptors, fftSize, hopSize, sampleRate, verbose = false|
		var cmd, path;
		path = Platform.defaultTempDir ++ UniqueID.next ++ ".npy";
		cmd = "cd" + soundMapPath.escapeChar($ ) ++ ";" + (this.python.python ?? { Python.pathToPython }) + "scripts/Analysis.py";
		cmd = cmd + soundPath.escapeChar($ );
		cmd = cmd + path.escapeChar($ );
		type !? { cmd = cmd + "-a" + type };
		descriptors !? { cmd = cmd + "-d"; descriptors.do{ |descriptor| cmd = cmd + descriptor } };
		fftSize !? { cmd = cmd + "-f" + fftSize };
		hopSize !? { cmd = cmd + "-h" + hopSize };
		sampleRate !? { cmd = cmd + "-s" + sampleRate };
		verbose.if({ cmd.postln });
		cmd.systemCmd;
		^python.fromNumpy(path, verbose)
	}

	soundSearchDTW { |soundPath, axes, verbose = false|
		var aData, dataMap;
		aData = this.modelAnalysisData(soundPath, verbose);
		dataMap = DataMap.new(data);
		^dataMap.nearestIndex(aData[0], axes)
	}

	soundSearchKD { |soundPath, axes, verbose = false|
		var aData, dataMap;
		axes = axes ?? { (0..data.shape[1]-1) };
		aData = this.modelAnalysisData(soundPath, verbose);
		dataMap = DataMap.new(data.collect{ |dat| dat[axes].flatten });
		^dataMap.nearestKDIndex(aData[0][axes].flatten)
	}

	dataSearch { |vals, axes, removeTime = false|
		var dataMap;
		dataMap = DataMap.new(removeTime.if({ this.removeTime }, { data }));
		^dataMap.nearestIndex(vals, axes)
	}

	removeTime { |data|
		data = data ?? { this.data };
		^(dataType == 'ml').if({
			data
		}, {
			data.collect{ |file| file.collect{ |feature| feature.sum/feature.size } }
		})
	}

	getFileDescriptor { |fileNum = 0, descriptor = 0|
		descriptor.isKindOf(String).if({ descriptor = this.descriptors.detectIndex{ |item| item == descriptor } });
		(this.rawData == "none").if({ "No raw data loaded".warn }, { ^this.rawData[fileNum][this.axes[descriptor]] });
	}

	normalize { |data|
		data = data ?? { this.data };
		^data.flop.collect{ |feature, i| feature.linlin(feature.minItem, feature.maxItem, -1, 1) }.flop
	}

	plot { |data, colors, func, close, parent, numSettings, loadOSC, showParticleSettings|
		window = parent ?? { Window.new("TSNE Sound Map", Window.screenBounds) };
		dataMapView = DataMapView.new(data, colors, func ?? { nearestFunc }, numSettings ?? { 8 }, window, loadOSC: loadOSC, showParticleSettings: showParticleSettings ?? { true });

		window.front;

		parent ?? {
			dataMapView.particleSettings !? {
				dataMapView.particleSettings.onClose_({
					window.close
				});
			}
		};

		dataMapView.cleanupFunc_(close ?? { onClose });

	}

	tsnePlot { |type = '3f', func, close, numSettings, loadOSC, showParticleSettings, parent|
		this.plot(this.perform(('tsne_' ++ type).asSymbol), this.perform(('colors_' ++ type).asSymbol), func, close, parent, numSettings, loadOSC, showParticleSettings)
	}

	axesPlot { |axes = ([0,1]), func, close, loadOSC|
		this.plot(this.normalize(this.removeTime).flop[axes].flop, nil, func, close, "Axes SoundMap", loadOSC)
	}

	setOSCAddr { |newAddr| this.dataMapView !? { this.dataMapView.resetOSCAddr(newAddr) } }

	writeJSON { |path, type|
		var file, coords = ["x", "y", "z"], points;

		points = this.perform(('tsne_' ++ type).asSymbol);

		(type != '3').if({ points = points.collect(_.asPoint) });

		points = points.collect(_.asArray);

		file = FileWriter(path);

		file.write("[");
		file.write("\n");


		points.do{ |arr, j|
			file.write("\t");
			file.write("{");
			file.write("\n");
			file.write("\t\t");
			file.write("id".asCompileString);
			file.write(": ");
			file.write(j.asCompileString);
			file.write(",");
			file.write("\n");
			arr.size.do{ |i|
				file.write("\t\t");
				file.write(coords[i].asCompileString);
				file.write(": ");
				file.write(arr[i].asCompileString);
				file.write(",");
				file.write("\n")
			};
			file.write("\t\t");
			file.write("color".asCompileString);
			file.write(": ");
			file.write(this.perform(('rawcolors_' ++ type).asSymbol)[j].asCompileString);
			file.write("\n");
			file.write("\t");
			file.write("}");
			(j < (points.size-1)).if({ file.write(",") });
			file.write("\n");
		};

		file.write("]");
		file.close
	}

	fluidAnalysisTransform { |features, fftSize = 1024, hopSize = 0.25, server|
		// features can be pitch, loudness, mfcc, stft (for now)
		var numFeatures = 0, bufs = Dictionary.new, featureBuf;
		var statsBuf, pointBuf, cond = Condition.new, dataSet, normalizer, umap, scaler, dataDict;
		var tmpColors;
		hopSize = (fftSize * hopSize).asInteger;
		features.do{ |feature|
			case
			{ feature == 'pitch' } { numFeatures = numFeatures + 1 }
			{ feature == 'loudness' } { numFeatures = numFeatures + 1 }
			{ feature == 'mfcc' } { numFeatures = numFeatures + 13 }
			{ feature == 'stft' } { numFeatures = numFeatures + (1 + (fftSize/2)) }
		};
		featureBuf = Buffer(server, numChannels: numFeatures);
		statsBuf = Buffer(server);
		pointBuf = Buffer.alloc(server, numFeatures);
		dataSet = FluidDataSet(server);
		normalizer = FluidNormalize(server);
		umap = FluidUMAP(server,2,15,0.1);
		scaler = FluidStandardize(server);

		server.sync;
		this.buffers.do{ |buf, i|
			var currentChan = 0;
			features.do{ |feature|
				case
				{ feature == 'pitch' }
				{
					bufs[feature] = Buffer(server, numChannels: 2);

					FluidBufPitch.processBlocking(server,buf,features: bufs[feature], windowSize: fftSize, hopSize: hopSize);
					FluidBufCompose.processBlocking(server, bufs[feature], destination: featureBuf, numChans: 1, destStartChan: currentChan);
					currentChan = currentChan + 1

				}
				{ feature == 'loudness' }
				{
					bufs[feature] = Buffer(server, numChannels: 2);
					FluidBufLoudness.processBlocking(server,buf,features: bufs[feature], windowSize: fftSize, hopSize: hopSize);
					FluidBufCompose.processBlocking(server, bufs[feature], destination: featureBuf, numChans: 1, destStartChan: currentChan);
					currentChan = currentChan + 1
				}
				{ feature == 'mfcc' }
				{
					bufs[feature] = Buffer(server, numChannels: 13);
					FluidBufMFCC.processBlocking(server,buf,features: bufs[feature], windowSize: fftSize, hopSize: hopSize, );
					FluidBufCompose.processBlocking(server, bufs[feature], destination: featureBuf, destStartChan: currentChan);
					currentChan = currentChan + 13
				}
				{ feature == 'stft' }
				{
					bufs[feature] = Buffer(server, numChannels: fftSize/2 + 1);
					FluidBufSTFT.processBlocking(server,buf,magnitude: bufs[feature], windowSize: fftSize, hopSize: hopSize);
					FluidBufCompose.processBlocking(server, bufs[feature], destination: featureBuf, destStartChan: currentChan);

				}
				{ feature == 'specShape' }
				{
					bufs[feature] = Buffer(server, numChannels: 7);
					FluidBufSpectralShape.processBlocking(server,buf,features: bufs[feature], windowSize: fftSize, hopSize: hopSize);
					FluidBufCompose.processBlocking(server, bufs[feature], destination: featureBuf, destStartChan: currentChan);
				};
			};
			bufs.do(_.free);
			server.sync;
			bufs = Dictionary.new;
			FluidBufStats.processBlocking(server,featureBuf,stats:statsBuf);
			FluidBufFlatten.processBlocking(server,statsBuf,startFrame:0,numFrames:1,destination:pointBuf, action: { cond.test_(true).signal });
			cond.wait;
			cond.test_(false);
			dataSet.addPoint(i,pointBuf);

		};

		server.sync;

		scaler.fitTransform(dataSet,dataSet); // perform all of these in place: overwrite the ~ds after each transformation
		umap.fitTransform(dataSet,dataSet);
		normalizer.fitTransform(dataSet,dataSet);

		dataSet.dump{|dict| dataDict = dict };
		server.sync;
		this.tsne_2 = Array.newClear(this.buffers.size);
		dataDict["data"].keysValuesDo{ |index, point|
			this.tsne_2[index.asInteger] = point
		};

		tmpColors = Platform.defaultTempDir +/+ "colors.npy";

		this.translateColors('2', this.kMeansColors(this.tsne_2, tmpColors)+1);

		this.tsne_2_(this.tsne_2.linlin(0, 1, -1, 1));

		File.delete(tmpColors)


	}

	gui {
		var loadData = false, loadFiles = true, loadDescriptors = false, loadTSNE = false, loadColors = false, verbose = false, type = '3f';
		var window;
		dataType = 'ml';
		window = Window.new("SoundMap", Window.screenBounds);
		window.layout_(
			VLayout(
				HLayout(
					Button(window).states_([
						["TSNE Plot", Color.black, Color.white]
					])
					.action_({ |butt|
						this.modelPath.notNil.if({
							this.read(this.modelPath, loadData, this.dataType, loadFiles, loadDescriptors, loadTSNE, loadColors, verbose);
							this.tsnePlot(type)
						}, {
							"Please set model path".warn
						})
					}),
					Button(window).states_([
						["Model Path", Color.black, Color.white]
					])
					.action_({ |butt|
						Dialog.openPanel({ |path|
							this.modelPath_(path) },
						path: Platform.userAppSupportDir);
					})
				),
				HLayout(
					Button(window).states_([
						["Data Type ML", Color.black, Color.white],
					])
					.action_({ |butt|
						dataType = 'ml'
					}),
					Button(window).states_([
						["Data Type Analysis", Color.black, Color.white]
					])
					.action_({ |butt|
						dataType = 'analysis'
					}),
					Button(window).states_([
						["2D", Color.black, Color.white]
					])
					.action_({ |butt|
						type = '2'
					}),
					Button(window).states_([
						["3Df", Color.black, Color.white]
					])
					.action_({ |butt|
						type = '3f'
					})
				),
				HLayout(
					Button(window).states_([
						["Load Data", Color.black, Color.white],
						["Load Data", Color.red, Color.white]
					])
					.action_({ |butt|
						loadData = butt.value.asBoolean
					}),
					Button(window).states_([
						["Load Descriptors", Color.black, Color.white],
						["Load Descriptors", Color.red, Color.white]
					])
					.action_({ |butt|
						loadDescriptors = butt.value.asBoolean
					}),
					Button(window).states_([
						["Load Files", Color.black, Color.white],
						["Load Files", Color.red, Color.white]
					])
					.action_({ |butt|
						loadFiles = butt.value.asBoolean
					}),
					Button(window).states_([
						["Load TSNE", Color.black, Color.white],
						["Load TSNE", Color.red, Color.white],
					])
					.action_({ |butt|
						loadTSNE = butt.value.asBoolean
					}),
					Button(window).states_([
						["Load Colors", Color.black, Color.white],
						["Load Colors", Color.red, Color.white]
					])
					.action_({ |butt|
						loadColors = butt.value.asBoolean
					}),
					Button(window).states_([
						["Verbose", Color.black, Color.white],
						["Verbose", Color.red, Color.white]
					])
					.action_({ |butt|
						verbose = butt.value.asBoolean
					})

				),
				Button(window).states_([
					["Load Buffers", Color.black, Color.white]
				])
				.action_({ |butt|
					this.files.notNil.if({
						this.buffers = this.files.collect{ |path| CtkBuffer(path).load(sync: false) };
					})
				})
			)
		);

		window.front

	}

	testPlot { |dim = 3|
		var window, buffers;
		var synths, points;
		var screenBounds, notesView;
		var server = Server.default;

		server.newAllocators;
		server.options.numBuffers_(files.size.nextPowerOfTwo);

		server.waitForBoot({

			screenBounds = Window.screenBounds;
			window = Window.new("MapTest", screenBounds);
			window.view.background_(Color.white);

			buffers = files.collect{ |path|
				CtkBuffer(path).load
			};

			points = (dim == 3).if({ this.tsne_3.linlin(0, 1, -1, 1) }, { this.tsne_2.linlin(0, 1, -1, 1) }).collect(_.asCartesian);

			notesView = PointView.new(window, window.bounds).points_(points).showIndices_(false).rotate_(0).tumble_(0.45pi).pointColors_(Color.black);

			window.front;

			synths = CtkProtoNotes(
				SynthDef('playBuf', {
					arg buffer, outbus, dur;
					var src, env;
					env = EnvGen.kr(Env([0, 1, 0], [0.01, dur - 0.02, 0.01], 'sin'));
					src = PlayBuf.ar(buffers[0].numChannels, buffer, BufRateScale.kr(buffer));
					Out.ar(outbus, src * env)
				})
			);

			notesView.userView.mouseDownAction_({
				|v,x,y, modifiers, buttonNumber, clickCount|
				var buffer, dur, nearestIndex, distances, mouseDownPnt;

				notesView.removeHighlight;
				mouseDownPnt = x@y;
				distances = notesView.pointsPx.collect{arg point;
					point.asPoint.dist(mouseDownPnt)
				};
				nearestIndex = distances.minIndex;
				notesView.pointsPx[nearestIndex];
				notesView.highlightPoints([nearestIndex]);
				buffer = buffers[nearestIndex];
				dur = buffer.duration;
				synths['playBuf'].note(duration: dur)
				.buffer_(buffer)
				.dur_(dur)
				.outbus_(0)
				.play;
			});

			notesView.userView.mouseMoveAction_({
				|v,x,y, modifiers, buttonNumber, clickCount|
				var buffer, dur, nearestIndex, distances, mouseDownPnt;

				notesView.removeHighlight;
				mouseDownPnt = x@y;
				distances = notesView.pointsPx.collect{arg point;
					point.asPoint.dist(mouseDownPnt)
				};
				nearestIndex = distances.minIndex;
				notesView.pointsPx[nearestIndex];
				notesView.highlightPoints([nearestIndex]);
				buffer = buffers[nearestIndex];
				dur = buffer.duration;
				synths['playBuf'].note(duration: dur)
				.buffer_(buffer)
				.dur_(dur)
				.outbus_(0)
				.play;
			});

			window.onClose_({
				CmdPeriod.run;
				buffers.do(_.free)
			});


		})
	}

	free { dataMapView !? { dataMapView.cleanup } }


}