SpectrogramPlot{
	var <parent, <data;
	var red, green, blue;
	var specView, gridView, imageView;

	*new { |parent, data|
		^super.newCopyArgs(parent, data).init
	}

	*read { |parent, path, sampleRate|
		sampleRate ?? { SoundFile.use(path, { |soundFile| sampleRate = soundFile.sampleRate }) };
		^super.newCopyArgs(parent, Analysis.new(sampleRate).fft(path, 2048, 0.5)).init
	}

	init {
		red = Env([0, 1, 1], [1, 1], 'sin');
		green = Env([0, 0, 1], [1, 1], 'sin');
		blue = Env([0, 0.3, 0], [0.25, 0.25], 'sin');
		parent = parent ?? { Window.new("Spectrogram", Window.screenBounds) };
		specView = UserView(parent, parent.bounds);
		specView.background_(Color.black);
		imageView = UserView(specView, specView.bounds);
		gridView = UserView(specView, specView.bounds).background_(Color.black);
	}

	plot {
		var image, colors, indices, bins;


		image = Image.new(data.shape[1], data.shape[0]);

		bins = data.shape[0];

		indices = (0..bins-1).linexp(0, bins-1, 1, bins).round.asInteger - 1;

		colors = data[indices].reverse.collect{ |bin|
			bin.collect{ |mag|
				mag = mag.explin(0.02, 1000, 0, 2, 'max');

				Image.colorToPixel(Color.new(red[mag],green[mag],blue[mag],1))
			}

		};

		colors.do{ |bin, y|
			bin.do{ |color, x| image.setPixel(color, x, y) }
		};
		image.interpolation_(\smooth); // uncomment to see the difference
		^image.plot("Spectrogram", Window.screenBounds, freeOnClose:true);



	}

	freqGrid {


		gridView.drawFunc = {
			DrawGrid(gridView.bounds, nil, \freq.asSpec.grid).gridColors_([Color.white, Color.white]).draw
		};

		specView.layout_(HLayout(imageView))


	}

	userView {
		var image, colors, indices, bins;

		image = Image.new(data.shape[1], data.shape[0]);

		bins = data.shape[0];

		indices = (0..bins-1).linexp(0, bins-1, 1, bins).round.asInteger - 1;

		colors = data[indices].reverse.collect{ |bin|
			bin.collect{ |mag|
				mag = mag.explin(0.02, 1000, 0, 2, 'max');

				Image.colorToPixel(Color.new(red[mag],green[mag],blue[mag],1))
			}

		};

		colors.do{ |bin, y|
			bin.do{ |color, x| image.setPixel(color, x, y) }
		};
		image.interpolation_(\smooth); // uncomment to see the difference
		imageView.drawFunc_({ Pen.drawImage(imageView.bounds, image, image.bounds); });


	}

}