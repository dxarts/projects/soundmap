import sys
from utils.bhtsne import bh_tsne
from utils.normalize import normalize
import numpy as np
from os.path import join

data_path = sys.argv[1]
data_path_2D = sys.argv[2]
data_path_3D = sys.argv[3]
initial_dims = int(sys.argv[4])
perplexity = int(sys.argv[5])
max_iter = int(sys.argv[6])

#  FOR TESTING
# data_path = '/Users/dan/Projects/SoundMaps/test2/Data/Data.npy'
data = np.load(data_path, allow_pickle=True)

min = np.min([len(file_data[0]) for file_data in data])
# new_data = []
# for inc,file_data in enumerate(data):
#     new_data.append([])
#     for feature in file_data:
#         indices = np.arange(0, len(feature), int(len(feature)/min))[:min]
#         new_data[inc].append([feature[index] for index in indices])
new_data = [feature[:min] for file_data in data for feature in file_data]
new_data = np.array(new_data)
new_data = new_data.reshape(data.shape[0], data.shape[1], min)
new_data = new_data.reshape(new_data.shape[0], -1)

print(new_data.shape)

X_2d = list(bh_tsne(new_data, initial_dims=initial_dims, perplexity=perplexity, no_dims=2, max_iter=max_iter, verbose=True))
X_2d = normalize(np.array(X_2d))
np.savetxt(data_path_2D, X_2d, fmt='%.5f', delimiter='\t')

X_3d = list(bh_tsne(new_data, initial_dims=initial_dims, perplexity=perplexity, no_dims=3, max_iter=max_iter))
X_3d = normalize(np.array(X_3d))
np.savetxt(data_path_3D, X_3d, fmt='%.5f', delimiter='\t')
