import numpy as np
import argparse
import joblib
import sys

from SoundMap.Audio_Analysis import Audio_Analysis
from SoundMap.AudioML import AudioML
from SoundMap.Utilities.Files import Files

parser = argparse.ArgumentParser(description='Use Audio_Analysis and send data to SC3')

parser.add_argument('sound_path', type=str,
                help='the path to the sound file to be analyzed')
parser.add_argument('data_path', type=str,
                help='the path to save the numpy data file')
parser.add_argument('-a', '--analysis_type', type=str, default='librosa',
                help='the analysis method, either essentia or librosa')
parser.add_argument('-d', '--descriptors', nargs='+', default=['spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness'],
                help='the descriptors')
parser.add_argument('-f', '--fft_size', type=int, default=2048
                help='the fft size of the analysis')
parser.add_argument('-h', '--hop_size', type=float, default=0.5,
                help='the hop size as a ratio of the fft size')
parser.add_argument('-s', '--sample_rate', type=int, default=48000,
                help='the sampling rate')


args = parser.parse_args()
sound_path = args.sound_path
data_path = args.data_path
analysis_type = args.analysis_type
descriptors = args.descriptors
fft_size = args.fft_size
hop_size = args.hop_size
sample_rate = args.sample_rate

audio_analysis = Audio_Analysis(files, fft_size, hop_size, sample_rate)

getattr(audio_analysis, analysis_type)(descriptors)

np.save(data_path, audio_analysis.data)
