import numpy as np
import argparse
import joblib
import sys
import os
import csv

from sklearn.cluster import MiniBatchKMeans


parser = argparse.ArgumentParser(description='Predict Based on Audio File Input')

parser.add_argument('data_path', type=str,
                help='the path to the xy data')
parser.add_argument('color_path', type=str,
                help='the path to save the color data')


args = parser.parse_args()

data_path = args.data_path
color_path = args.color_path
rows = []
with open(data_path) as f:
    csvreader = csv.reader(f)
    for row in csvreader:
        rows.append([float(ele) for ele in row])


kmeans = MiniBatchKMeans(n_clusters=8, random_state=0, batch_size=6, max_iter=10)
colors = kmeans.fit(rows).labels_
np.save(color_path, colors)
