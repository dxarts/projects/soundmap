import numpy as np
import argparse
import joblib
import sys
import os

from SoundMap.Audio_Analysis import Audio_Analysis
from SoundMap.AudioML import AudioML
from SoundMap.Utilities.Files import Files

parser = argparse.ArgumentParser(description='Predict Based on Audio File Input')

parser.add_argument('audioML_path', type=str,
                help='the path to the saved instance of AudioML')
parser.add_argument('audio_path', type=str,
                help='the path to the prediction audio file')

args = parser.parse_args()

audioML_path = args.audioML_path
audio_path = args.audio_path

std_out = sys.stdout

audioML = joblib.load(audioML_path)

audio_analysis = audioML.audio_analysis

audio_analysis.files = Files(audio_path)
audio_analysis.essentia(audio_analysis.descriptors)

audio_analysis.data = audio_analysis.scale(audio_analysis.scale_types)

audio_analysis.data = audio_analysis.interpolate(audio_analysis.length)

pred = audioML.dtw(audio_analysis.data)
if isinstance(pred, np.ndarray):
    pred = pred[0]

std_out.write(str(pred))
