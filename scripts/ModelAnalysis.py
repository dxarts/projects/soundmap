import numpy as np
import argparse
import joblib
import sys

from SoundMap.Audio_Analysis import Audio_Analysis
from SoundMap.AudioML import AudioML
from SoundMap.Utilities.Files import Files

parser = argparse.ArgumentParser(description='Use Audio_Analysis and send data to SC3')

parser.add_argument('sound_path', type=str,
                help='the path to the sound file to be analyzed')
parser.add_argument('model_path', type=str,
                help='the path to the audioML instance')
parser.add_argument('data_path', type=str,
                help='the path to save the numpy data file')
parser.add_argument('-d', '--data_type', type=str, default='ml',
                help='either the analysis or ml data')


args = parser.parse_args()
sound_path = args.sound_path
data_path = args.data_path
audioML_path = args.model_path
data_type = args.data_type

audioML = joblib.load(audioML_path)

audio_analysis = audioML.audio_analysis
audio_analysis.files = Files(sound_path)

getattr(audio_analysis, audio_analysis.type)(audio_analysis.descriptors)
audio_analysis.data = audio_analysis.scale(audio_analysis.scale_types)

audio_analysis.data = audio_analysis.interpolate(audio_analysis.length)

if data_type == 'ml':
    np.save(data_path, audioML.pca_model.transform(audioML.prepare_data(audio_analysis.data)))
else:
    np.save(data_path, audio_analysis.data)
