import numpy as np
import argparse
import joblib
import sys

from SoundMap.Audio_Analysis import Audio_Analysis
from SoundMap.AudioML import AudioML
from SoundMap.Utilities.Files import Files

parser = argparse.ArgumentParser(description='Read Audio_Analysis and AudioML Data into SC')

parser.add_argument('audioML_path', type=str,
                help='the path to the saved instance of AudioML')
parser.add_argument('data_path', type=str,
                help='the path to save the numpy data file')
parser.add_argument('file_path', type=str,
                help='the path to save the csv files file')
parser.add_argument('descriptors_path', type=str,
                help='the path to save the csv descriptors')
parser.add_argument('tsne_2_path', type=str,
                help='the path to save the csv tsne_2')
parser.add_argument('tsne_3_path', type=str,
                help='the path to save the csv tsne_3')
parser.add_argument('tsne_3f_path', type=str,
                help='the path to save the csv tsne_3f')
parser.add_argument('tsne_3u_path', type=str,
                help='the path to save the csv tsne_3u')
parser.add_argument('colors_path_2', type=str,
                help='the path to save the csv 2D colors')
parser.add_argument('colors_path_3', type=str,
                help='the path to save the csv 3D colors')
parser.add_argument('colors_path_3f', type=str,
                help='the path to save the csv 3D unfolder colors')
parser.add_argument('colors_path_3u', type=str,
                help='the path to save the csv 3D unit sphere colors')
parser.add_argument('-d', '--data_type', type=str, default='ml',
                help='either the analysis or ml data')

args = parser.parse_args()

audioML_path = args.audioML_path
data_path = args.data_path
file_path = args.file_path
descriptors_path = args.descriptors_path
tsne_2_path = args.tsne_2_path
tsne_3_path = args.tsne_3_path
tsne_3f_path = args.tsne_3f_path
tsne_3u_path = args.tsne_3u_path
colors_path_2 = args.colors_path_2
colors_path_3 = args.colors_path_3
colors_path_3f = args.colors_path_3f
colors_path_3u = args.colors_path_3u
data_type = args.data_type

audioML = joblib.load(audioML_path)

audio_analysis = audioML.audio_analysis

if data_type == 'ml':
    np.save(data_path, audioML.data)
else:
    np.save(data_path, audio_analysis.data)
np.savetxt(file_path, audio_analysis.files.relative_files, delimiter=',', fmt="%s")
np.savetxt(descriptors_path, audio_analysis.descriptors, delimiter=',', fmt="%s")
np.savetxt(tsne_2_path, audioML.tsne_2, fmt='%.5f', delimiter=',')
np.savetxt(tsne_3_path, audioML.tsne_3, fmt='%.5f', delimiter=',')
np.savetxt(tsne_3f_path, audioML.tsne_3f, fmt='%.5f', delimiter=',')
np.savetxt(tsne_3u_path, audioML.tsne_3u, fmt='%.5f', delimiter=',')
np.savetxt(colors_path_2, audioML.colors_2, fmt='%.5f', delimiter=',')
np.savetxt(colors_path_3, audioML.colors_3, fmt='%.5f', delimiter=',')
np.savetxt(colors_path_3f, audioML.colors_3f, fmt='%.5f', delimiter=',')
np.savetxt(colors_path_3u, audioML.colors_3u, fmt='%.5f', delimiter=',')