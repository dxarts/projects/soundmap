import joblib
import numpy as np
from SoundMap.AudioML import AudioML
from SoundMap.Audio_Analysis import Audio_Analysis
from SoundMap.Utilities.Scaler import Scaler
import os
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description='Run Audio Analysis from SC3')

parser.add_argument('sound_map_path', type=str,
                help='the path to the sound map folder')
parser.add_argument('-a', '--analysis_type', type=str, default='librosa',
                help='the analysis method, either essentia or librosa')
parser.add_argument('-d', '--descriptors', nargs='+', default=['spectral_energy', 'spectral_centroid', 'spectral_rolloff', 'spectral_flatness'],
                help='the descriptors')
parser.add_argument('-f', '--fft_size', type=int, default=8192,
                help='the fft window size')
parser.add_argument('-p', '--hop_size', type=float, default=0.25,
                help='the hop size as percentage of fft_size')
parser.add_argument('-s', '--sample_rate', type=int, default=48000,
                help='the sample rate')
parser.add_argument('-i', '--interpolate_length', type=int, default=100,
                help='interpolate length')

args = parser.parse_args()
sound_map_path = args.sound_map_path
analysis_type = args.analysis_type
descriptors = args.descriptors
fft_size = args.fft_size
hop_size = args.hop_size
sample_rate = args.sample_rate
interpolate_length = args.interpolate_length

files = os.path.join(sound_map_path, "samples")

# (sound path, fft size, hop size as ratio of fft size, sample rate)
audio_analysis = Audio_Analysis(files, fft_size, hop_size, sample_rate)

# run the analysis using librosa, setting the array of descriptors
getattr(audio_analysis, analysis_type)(descriptors)

# scale the data, using log normalization for the first feature and z-score normalization (StandardScaler) for the next two
audio_analysis.data = audio_analysis.scale()

# interpolate the data to make each file and feature the same length
audio_analysis.data = audio_analysis.interpolate(interpolate_length)

# define the "SoundMap"
audioML = AudioML(audio_analysis)

# flatten the data
audioML.data = audioML.prepare_data()

# generate the 2D and 3D maps
audioML.tsne(10, 100, 1000)

# define and train the neural network
audioML.define_MLP([1000, 1000, 1000], 10000, 'adam', 0.001, 0.001)
audioML.train()

# save the audioML for later
save_path = os.path.abspath(os.path.join(files, os.pardir, "Data", analysis_type + ".sav"))
audioML.save(save_path)
